/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import org.randombits.filtering.core.criteria.Criterion;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.renderer.links.Link;

/**
 * Checks if the content has been trashed. This criterion can check to make sure
 * content is either deleted or not deleted.
 * 
 * @author David Peterson
 */
public class DeletedContentCriterion implements Criterion {
    private boolean isDeleted = false;

    /**
     * Constructs the criterion.
     * 
     * @param isDeleted
     *            If <code>true</code> the criterion will match content which
     *            has been deleted.
     */
    public DeletedContentCriterion( boolean isDeleted ) {
        this.isDeleted = isDeleted;
    }

    public boolean matches( Object object ) {
       if ( object instanceof Link ) {
            if ( object instanceof AbstractContentEntityLink ) {
                object = ( ( AbstractContentEntityLink ) object ).getDestinationContent();
            } else if ( object instanceof AttachmentLink ) {
                object = ( ( AttachmentLink ) object ).getAttachment().getContent();
            } else {
                return true;
            }
        }

        if ( object instanceof ContentEntityObject ) {
            return isDeleted == ( ( ContentEntityObject ) object ).isDeleted();
        }
        return !isDeleted;
    }

    @Override
    public String toString() {
        return "{deleted: " + isDeleted + "}";
    }

}
