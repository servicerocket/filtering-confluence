/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.filtering.confluence.criteria.user;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;

import java.util.Map;

/**
 * Checks if the object is a User and it has the specified space permission.
 */
public class SpacePermissionCriterion extends AutowiredCriterion {

    public static class Interpreter implements CriterionInterpreter {

        /**
         * A post-fix for specifying a user must have 'edit' permissions for a
         * spaces.
         */
        public static final String EDIT_PAGE = "edit";

        /**
         * A post-fix for specifying a user must have 'admin' permissions for a
         * space.
         */
        public static final String SPACE_ADMIN = "admin";

        // This is private because blog has been deprecated in favour of news.
        private static final String SPACE_BLOG = "blog";

        /**
         * A post-fix for specifying a user must have 'news' permissions for a
         * space.
         */
        public static final String SPACE_NEWS = "news";

        /**
         * A post-fix for specifying a user must have 'view' permissions for a
         * space.
         */
        public static final String SPACE_VIEW = "view";

        /**
         * A post-fix for specifying a user must have 'comment' permissions for
         * a space.
         */
        public static final String SPACE_COMMENT = "comment";

        private static final String SELF_SPACE = "@space";

        private static final Map<String, Object> ACTIONS;

        static {
            ACTIONS = new java.util.HashMap<String, Object>( 4 );
            ACTIONS.put( SPACE_VIEW, Permission.VIEW );
            ACTIONS.put( SPACE_ADMIN, Permission.ADMINISTER );

            ACTIONS.put( EDIT_PAGE, Page.class );
            ACTIONS.put( SPACE_BLOG, BlogPost.class );
            ACTIONS.put( SPACE_NEWS, BlogPost.class );
            ACTIONS.put( SPACE_COMMENT, Comment.class );
        }

        private String spaceKey;

        public Interpreter( Space space ) {
            this( space == null ? null : space.getKey() );
        }

        public Interpreter( String spaceKey ) {
            this.spaceKey = spaceKey;
        }

        public String getSpaceKey() {
            return spaceKey;
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            Object action;

            String spaceKey = value;

            // Find the separator.
            int splitAt = spaceKey.indexOf( '>' );
            if ( splitAt == -1 )
                splitAt = spaceKey.indexOf( ':' );

            if ( splitAt != -1 ) {
                action = ACTIONS.get( spaceKey.substring( splitAt + 1 ).toLowerCase().trim() );
                spaceKey = spaceKey.substring( 0, splitAt ).trim();

                if ( action == null )
                    throw new IllegalArgumentException( "The permission requested is not recognised: " + value );
            } else {
                action = Permission.VIEW;
            }

            if ( SELF_SPACE.equals( spaceKey ) ) {
                if ( this.spaceKey != null )
                    spaceKey = this.spaceKey;
                else
                    throw new CriteriaException( SELF_SPACE + " cannot be used in this context." );
            }

            if ( action instanceof Permission ) {
                return new SpacePermissionCriterion( spaceKey, (Permission) action );
            } else if ( action instanceof Class ) {
                return new SpacePermissionCriterion( spaceKey, (Class<?>) action );
            } else {
                throw new CriteriaException( "Unsupported space permission: " + value );
            }
        }

    }

    private PermissionManager permissionManager;

    private String spaceKey;

    private Permission permission;

    private Class<?> createClass;

    private SpaceManager spaceManager;

    private UserAccessor userAccessor;

    /**
     * Constructs a new criterion which will check that the object is a User who
     * can create the specified class in the specified space.
     *
     * @param spaceKey    The space to check.
     * @param createClass The class to create.
     */
    public SpacePermissionCriterion( String spaceKey, Class<?> createClass ) {
        this.spaceKey = spaceKey;
        this.createClass = createClass;
    }

    /**
     * Constructs a new criterion with the specified space key and space
     * permission type (e.g. {@link com.atlassian.confluence.security.Permission#VIEW})
     *
     * @param spaceKey   The space key.
     * @param permission The space permission type.
     */
    public SpacePermissionCriterion( String spaceKey, Permission permission ) {
        this.spaceKey = spaceKey;
        this.permission = permission;
    }

    public boolean matches( Object object ) {
        User user = null;

        if ( object instanceof User )
            user = (User) object;
        else if ( object instanceof String )
            user = userAccessor.getUser( (String) object );
        else if ( object != null )
            return false;

        // Admins have all permissions on spaces, but are often
        // not given them explicitly.
        if ( permissionManager.hasPermission( user, Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION ) )
            return true;

        Space space = spaceManager.getSpace( spaceKey );
        if ( space != null ) {
            if ( permission != null )
                return permissionManager.hasPermission( user, permission, space );
            else
                return permissionManager.hasCreatePermission( user, space, createClass );
        }
        return false;
    }

    public void setPermissionManager( PermissionManager permissionManager ) {
        this.permissionManager = permissionManager;
    }

    public void setSpaceManager( SpaceManager spaceManager ) {
        this.spaceManager = spaceManager;
    }

    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public Permission getPermission() {
        return permission;
    }

    @Override
    public String toString() {
        return "{space: " + spaceKey
                + ( createClass == null ? "" : "; create class: " + createClass.getSimpleName() )
                + ( permission == null ? "" : "; permission: " + permission ) + "}";
    }
}
