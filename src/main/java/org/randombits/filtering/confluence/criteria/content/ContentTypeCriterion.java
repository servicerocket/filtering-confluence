/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.links.linktypes.PageCreateLink;
import com.atlassian.confluence.links.linktypes.ShortcutLink;
import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResult;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.links.UrlLink;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;

import java.util.Map;

/**
 * Matches the content to the specified content types.
 * 
 * @author David Peterson
 */
public class ContentTypeCriterion implements Criterion {
    public static class Interpeter implements CriterionInterpreter {
        private static final Map<String, String> TYPES = new java.util.HashMap<String, String>();

        static {
            TYPES.put( "page", Page.CONTENT_TYPE );
            TYPES.put( "blogpost", BlogPost.CONTENT_TYPE );
            TYPES.put( "news", BlogPost.CONTENT_TYPE );
            TYPES.put( "spacedescription", SpaceDescription.CONTENT_TYPE_SPACEDESC );
            TYPES.put( "comment", Comment.CONTENT_TYPE );
            TYPES.put( "attachment", Attachment.CONTENT_TYPE );
            TYPES.put( "mail", Mail.CONTENT_TYPE );

            TYPES.put( "space", ContentTypeCriterion.SPACE_TYPE );

            TYPES.put( "mailto", ContentTypeCriterion.MAILTO_TYPE );
            TYPES.put( "url", ContentTypeCriterion.URL_TYPE );
            TYPES.put( "unresolved", ContentTypeCriterion.UNRESOLVED_TYPE );
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            String type = TYPES.get( value.toLowerCase() );
            if ( type == null )
                throw new CriteriaException( "Unsupported content type: " + value );
            return new ContentTypeCriterion( type );
        }
    };

    private String contentType;

    public static final String UNRESOLVED_TYPE = "unresolved";

    public static final String MAILTO_TYPE = "mailto";

    public static final String URL_TYPE = "url";

    public static final String SPACE_TYPE = "space";

    public ContentTypeCriterion( String contentType ) {
        this.contentType = contentType;
    }

    public boolean matches( Object object ) {
        String objectType = null;
        
        if (object instanceof SearchResult){
        	 objectType = ((SearchResult) object).getType(); 
        } else if ( object instanceof UnpermittedLink ) {
            object = ( ( UnpermittedLink ) object ).getWrappedLink();
        } else if ( object instanceof AbstractContentEntityLink ) {
            if ( object instanceof PageCreateLink )
                objectType = Page.CONTENT_TYPE;
            else
                object = ( ( AbstractContentEntityLink ) object ).getDestinationContent();
        } else if ( object instanceof AttachmentLink ) {
            object = ( ( AttachmentLink ) object ).getAttachment();
        } else if ( object instanceof UnresolvedLink ) {
            objectType = UNRESOLVED_TYPE;
        } else if ( object instanceof UrlLink ) {
            UrlLink urlLink = ( UrlLink ) object;
            if ( isMailLink( urlLink ) ) // urlLink.isMailLink()
                objectType = MAILTO_TYPE;
            else
                objectType = URL_TYPE;
        } else if ( object instanceof ShortcutLink ) {
            objectType = URL_TYPE;
        }

        if ( object instanceof ContentEntityObject ) {
            ContentEntityObject content = ( ContentEntityObject ) object;
            objectType = content.getType();
        } else if ( object instanceof Attachment ) {
            objectType = ( ( Attachment ) object ).getType();
        } else if ( object instanceof Space ) {
            objectType = SPACE_TYPE;
        } else if ( object instanceof String ) {
            // handles the case where the object type is passed in directly
            objectType = ( String ) object;
        }

        return contentType.equalsIgnoreCase( objectType );
    }

    public String getContentType() {
        return contentType;
    }

    public static boolean isMailLink( UrlLink link ) {
        return link.getUrl().startsWith( "mailto:" );
    }

    @Override
    public String toString() {
        return "{content type: " + contentType + "}";
    }
}
