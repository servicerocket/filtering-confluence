/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.links.linktypes.PageCreateLink;
import com.atlassian.confluence.links.linktypes.ShortcutLink;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.links.UrlLink;
import com.atlassian.user.User;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;

/**
 * Determines if the supplied object matches the permissions for the user.
 * 
 * @author David Peterson
 */
public class ContentPermissionCriterion extends AutowiredCriterion {
    private User user;

    private boolean hasPermission;

    private PermissionManager permissionManager;

    private Permission permission;

    private SpaceManager spaceManager;

    public ContentPermissionCriterion( User user, Permission permission, boolean hasPermission ) {
        this.user = user;
        this.permission = permission;
        this.hasPermission = hasPermission;
    }

    public ContentPermissionCriterion( User user, Permission permission ) {
        this( user, permission, true );
    }

    public boolean matches( Object object ) {
        if ( object instanceof Link ) {
            if ( object instanceof AbstractContentEntityLink ) {
                if ( object instanceof PageCreateLink ) {
                    String spaceKey = ( ( PageCreateLink ) object ).getSpaceKey();
                    if ( spaceKey != null )
                        object = spaceManager.getSpace( spaceKey );

                } else {
                    object = ( ( AbstractContentEntityLink ) object ).getDestinationContent();
                }
            } else if ( object instanceof AttachmentLink ) {
                object = ( ( AttachmentLink ) object ).getAttachment();
            } else if ( object instanceof UnpermittedLink ) {
                return false;
            } else
                return object instanceof UnresolvedLink || object instanceof ShortcutLink
                        || object instanceof UrlLink;
        }

        if ( object != null )
            return permissionManager.hasPermission( user, permission, object ) == hasPermission;

        return false;
    }

    /**
     * The user to check against.
     * 
     * @return The user to check for.
     */
    public User getUser() {
        return user;
    }

    /**
     * @return <code>true</code> if this criterion checks that the user can
     *         view.
     */
    public boolean isHasPermission() {
        return hasPermission;
    }

    /**
     * @return the permission to check for.
     */
    public Permission getPermission() {
        return permission;
    }

    public void setPermissionManager( PermissionManager permissionManager ) {
        this.permissionManager = permissionManager;
    }

    public void setSpaceManager( SpaceManager spaceManager ) {
        this.spaceManager = spaceManager;
    }

    @Override
    public String toString() {
        return "{content permission: " + permission + "; user: " + user + "; must have permission: "
                + hasPermission + "}";
    }
}
