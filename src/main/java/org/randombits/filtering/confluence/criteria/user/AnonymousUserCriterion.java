package org.randombits.filtering.confluence.criteria.user;

public class AnonymousUserCriterion extends UserCriterion {

    public AnonymousUserCriterion() {
        super( ( String ) null );
    }

}
