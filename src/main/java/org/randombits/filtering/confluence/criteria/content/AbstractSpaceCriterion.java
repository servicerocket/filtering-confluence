package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.links.linktypes.PageCreateLink;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.links.Link;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;

public abstract class AbstractSpaceCriterion extends AutowiredCriterion {

    protected SpaceManager spaceManager;

    protected abstract boolean matchesSpace( Space space );

    public AbstractSpaceCriterion() {
        super();
    }

    public boolean matches( Object object ) {
        Space space = null;
        
        if(object instanceof SearchResult) 
        	space =  spaceManager.getSpace(((SearchResult)object).getSpaceKey());
        
        if ( object instanceof Link ) {
            if ( object instanceof AbstractContentEntityLink ) {
                if ( object instanceof PageCreateLink ) {
                    String spaceKey = ( ( PageCreateLink ) object ).getSpaceKey();
                    if ( spaceKey != null )
                        space = spaceManager.getSpace( spaceKey );
                } else {
                    object = ( ( AbstractContentEntityLink ) object ).getDestinationContent();
                }
            } else if ( object instanceof AttachmentLink ) {
                object = ( ( AttachmentLink ) object ).getAttachment();
            } else {
                // Always return true for non-spaced destinations.
                return true;
            }
        }

        if ( object instanceof Attachment ) {
            object = ( ( Attachment ) object ).getContent();
        }

        if ( object instanceof SpaceContentEntityObject ) {
            SpaceContentEntityObject ceo = ( SpaceContentEntityObject ) object;
            space = ceo.getSpace();
        } else if ( object instanceof Space ) {
            space = ( Space ) object;
        }

        if ( space != null ) {
            return matchesSpace( space );
        }

        return matchesNullSpace();
    }

    protected boolean matchesNullSpace() {
        return false;
    }

    public void setSpaceManager( SpaceManager spaceManager ) {
        this.spaceManager = spaceManager;
    }

}