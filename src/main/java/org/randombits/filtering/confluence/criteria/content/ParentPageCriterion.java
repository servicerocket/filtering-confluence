/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.filtering.core.criteria.SourceCriterion;
import org.randombits.support.confluence.LinkAssistant;

import java.util.Collection;
import java.util.Collections;

/**
 * This criterion will match if the object is a Page and its ID matches that
 * specified.
 *
 * @author David Peterson
 */
public class ParentPageCriterion extends AutowiredCriterion implements SourceCriterion<Page> {

    public static class Interpreter implements CriterionInterpreter {

        /**
         * The key for 'current content' - "@self".
         */
        public static final String SELF = "@self";

        public static final String PARENT = "@parent";

        private ContentEntityObject self;

        private final LinkAssistant linkAssistant;

        public Interpreter( LinkAssistant linkAssistant ) {
            this( linkAssistant, null );
        }

        public Interpreter( LinkAssistant linkAssistant, ContentEntityObject self ) {
            this.self = self;
            this.linkAssistant = linkAssistant;
        }

        /**
         * Returns the content which will be used for {@link #SELF} or
         * {@link #PARENT} references.
         *
         * @return The space for "@self".
         */
        public ContentEntityObject getSelf() {
            return self;
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            Page page = null;
            if ( SELF.equals( value ) ) {
                // Set the page to the 'self'.
                page = asPage( self, value );
            } else if ( PARENT.equals( value ) ) {
                page = asPage( self, value ).getParent();
            } else {
                ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( new DefaultConversionContext( self.toPageContext() ),
                        value );
                page = asPage( entity, value );
            }

            if ( page != null )
                throw new CriteriaException( "'" + value + "' could not be found in this context." );

            return new ParentPageCriterion( page );
        }

        private Page asPage( ConfluenceEntityObject content, String value ) throws CriteriaException {
            if ( content instanceof Page )
                return (Page) content;
            else
                throw new CriteriaException( "'" + value + "' could not be found in this context." );
        }
    }

    private long parentPageId = 0;

    private PageManager pageManager;

    public ParentPageCriterion( long pageId ) {
        this.parentPageId = pageId;
    }

    public ParentPageCriterion( Page parentPage ) {
        this( parentPage.getId() );
    }

    public boolean matches( Object object ) {
        if ( object instanceof PageLink ) {
            object = ( (PageLink) object ).getDestinationContent();
        }

        if ( object instanceof Page ) {
            Page page = (Page) object;
            Page parentPage = page.getParent();

            return parentPage != null && parentPage.getId() == parentPageId;
        }

        return false;
    }

    public void setPageManager( PageManager pageManager ) {
        this.pageManager = pageManager;
    }

    /**
     * The unique ID of the parent page.
     *
     * @return the parent page ID.
     */
    public long getParentPageId() {
        return parentPageId;
    }

    @Override
    public String toString() {
        return "{parent page id: " + parentPageId + "}";
    }

    public Collection<Page> getMatchingValues() {
        Page parent = pageManager.getPage( parentPageId );
        if ( parent != null ) {
            return parent.getChildren();
        }

        return Collections.EMPTY_LIST;
    }

    public SourceCriterion.Weight getWeight() {
        return Weight.LIGHT;
    }
}
