/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.*;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.filtering.core.criteria.SourceCriterion;

import java.util.Collection;

/**
 * Checks if the supplied object has the required label.
 *
 * @author David Peterson
 */
public class LabelCriterion extends AutowiredCriterion implements SourceCriterion<ContentEntityObject> {

    /**
     * This is an interpretor that will produce label criterion.
     *
     * @author David Peterson
     */
    public static class Interpreter implements CriterionInterpreter {

        public Criterion createCriterion( String value ) {
            return new LabelCriterion( value );
        }
    }

    private ParsedLabelName labelName;

    private LabelManager labelManager;

    public LabelCriterion( ParsedLabelName labelName ) {
        this.labelName = labelName;
        makeGlobalIfNecessary();
    }

    private void makeGlobalIfNecessary() {
        if ( labelName == null )
            return;
        if ( StringUtils.isBlank( labelName.getOwner() ) && StringUtils.isBlank( labelName.getPrefix() ) )
            labelName.setPrefix( Namespace.GLOBAL.getPrefix() );
    }

    public LabelCriterion( String labelName ) {
        this( LabelParser.parse( labelName ) );
    }

    public boolean matches( Object object ) {
        if (object instanceof SearchResult) {
            final User currentUser = AuthenticatedUserThreadLocal.getUser();
            if (labelName != null) {
                try {
                    return ((SearchResult) object).getLabels(currentUser).contains(labelName.getName());
                } catch (Exception ex) {
                    // do nothing
                }
            }
        }
        if (object instanceof AbstractContentEntityLink) {
            object = ((AbstractContentEntityLink) object).getDestinationContent();
        }
        // Space labels actually get attached to their SpaceDescription object.
        if (object instanceof Space)
            object = ((Space) object).getDescription();

        if (object instanceof Labelable) {
            Labelable labelable = (Labelable) object;
            Label label = getLabel();
            Label labelTeam = getLabelByNameSpace(Namespace.TEAM);
            if (label != null || labelTeam != null) {
                return labelable.getLabels().contains(label) || labelable.getLabels().contains(labelTeam);
            }
        }
        return false;
    }

    public void setLabelManager( LabelManager labelManager ) {
        this.labelManager = labelManager;
    }

    @Override
    public String toString() {
        return "{has label: " + ( labelName.getPrefix() != null ? labelName.getPrefix() + ":" : "" )
                + labelName.getName() + "}";
    }

    public Collection<ContentEntityObject> getMatchingValues() {
        Label label = getLabel();
        if ( label != null )
            return labelManager.getContent( label );
        return null;
    }

    private Label getLabelByNameSpace(Namespace namespace) {
        return labelManager.getLabel(labelName.getName(), namespace);
    }

    private Label getLabel() {
        return labelManager.getLabel( labelName );
    }

    public SourceCriterion.Weight getWeight() {
        Label label = getLabel();
        if ( label != null ) {
            int count = labelManager.getContentCount( label );
            if ( count < 10 )
                return Weight.LIGHT;
            else if ( count < 100 )
                return Weight.MEDIUM;
            else
                return Weight.HEAVY;
        }
        return Weight.LIGHT;
    }
}
