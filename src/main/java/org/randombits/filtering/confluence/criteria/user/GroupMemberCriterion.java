/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.filtering.confluence.criteria.user;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import org.randombits.filtering.confluence.criteria.AutowiredCriterion;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.filtering.core.criteria.SourceCriterion;

import java.util.Collection;
import java.util.Collections;

/**
 * Checks if the object is a {@link com.atlassian.user.User} and is a member of
 * the specified group.
 */
public class GroupMemberCriterion extends AutowiredCriterion implements SourceCriterion<String> {
    public static class Interpreter implements CriterionInterpreter {

        public Criterion createCriterion( String value ) {
            return new GroupMemberCriterion( value );
        }

    }

    private String groupName;

    private UserAccessor userAccessor;

    public GroupMemberCriterion( String groupName ) {
        this.groupName = groupName;
    }

    public GroupMemberCriterion( Group group ) {
        this.groupName = group.getName();
    }

    public boolean matches( Object object ) {
        String username = null;
        if ( object instanceof User ) {
            username = ( ( User ) object ).getName();
        } else if ( object instanceof String ) {
            username = ( String ) object;
        }

        if ( username != null )
            return userAccessor.hasMembership( groupName, username );

        return false;
    }

    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }

    /**
     * Returns the list of usernames in the group matching this criterion.
     */
    public Collection<String> getMatchingValues() {
        Group group = userAccessor.getGroup( groupName );
        if ( group == null )
            return Collections.EMPTY_LIST;
        return userAccessor.getMemberNamesAsList( group );
    }

    @Override
    public String toString() {
        return "{group member: " + groupName + "}";
    }

    public SourceCriterion.Weight getWeight() {
        return Weight.MEDIUM;
    }
}
