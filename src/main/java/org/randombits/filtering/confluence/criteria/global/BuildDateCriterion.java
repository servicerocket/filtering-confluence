/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.global;

import com.atlassian.confluence.util.GeneralUtil;
import org.randombits.filtering.core.criteria.Criterion;

import java.util.Date;

/**
 * Checks if the specified object is a {@link java.util.Date} and that it
 * matches the specified settings.
 * 
 * @author David Peterson
 */
public class BuildDateCriterion implements Criterion {
    private Date minValue;

    private Date maxValue;

    public BuildDateCriterion() {
    }

    public BuildDateCriterion( Date minValue, Date maxValue ) {
        this();
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public boolean matches( Object object ) {
        return matchesType( GeneralUtil.getBuildDate() );
    }

    /**
     * Checks if the object matches this criterion.
     * 
     * @param date
     *            The object.
     * @return <code>true</code> if the object matches this criterion.
     */
    private boolean matchesType( Date date ) {
        if ( minValue != null && minValue.after( date ) )
            return false;

        if ( maxValue != null && maxValue.before( date ) )
            return false;

        return true;
    }

    public Date getMinValue() {
        return minValue;
    }

    public void setMinValue( Date minValue ) {
        this.minValue = minValue;
    }

    public Date getMaxValue() {
        return maxValue;
    }

    public void setMaxValue( Date maxValue ) {
        this.maxValue = maxValue;
    }

    @Override
    public String toString() {
        return "{build date" + ( minValue != null ? "; min: " + minValue : "" )
                + ( maxValue != null ? "; max: " + maxValue : "" ) + "}";
    }
}
