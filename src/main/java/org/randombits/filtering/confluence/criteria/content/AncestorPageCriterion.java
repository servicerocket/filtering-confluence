/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.pages.Page;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;

/**
 * Determines if the specified ancestor page is actually an ancestor of the
 * supplied object.
 *
 * @author David Peterson
 */
public class AncestorPageCriterion implements Criterion {

    public static class Interpreter implements CriterionInterpreter {

        /**
         * The key for 'current content' - "@self".
         */
        public static final String SELF = "@self";

        public static final String PARENT = "@parent";

        private ContentEntityObject self;

        private final LinkAssistant linkAssistant;

        public Interpreter( LinkAssistant linkAssistant ) {
            this( linkAssistant, null );
        }

        public Interpreter( LinkAssistant linkAssistant, ContentEntityObject self ) {
            this.self = self;
            this.linkAssistant = linkAssistant;
        }

        /**
         * Returns the content which will be used for {@link #SELF} or
         * {@link #PARENT} references.
         *
         * @return The space for "@self".
         */
        public ContentEntityObject getSelf() {
            return self;
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            Page page = null;
            if ( SELF.equals( value ) ) {
                // Set the page to the 'self'.
                page = asPage( self, value );
            } else if ( PARENT.equals( value ) ) {
                page = asPage( self, value ).getParent();
            } else {
                ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( new DefaultConversionContext( self.toPageContext() ),
                        value );
                page = asPage( entity, value );
            }

            if ( page != null )
                throw new CriteriaException( "'" + value + "' could not be found in this context." );

            return new AncestorPageCriterion( linkAssistant, page );
        }

        private Page asPage( ConfluenceEntityObject content, String value ) throws CriteriaException {
            if ( content instanceof Page )
                return (Page) content;
            else
                throw new CriteriaException( "'" + value + "' could not be found in this context." );
        }
    }

    private long ancestorPageId;

    private final LinkAssistant linkAssistant;

    public AncestorPageCriterion( LinkAssistant linkAssistant, long ancestorPageId ) {
        this.ancestorPageId = ancestorPageId;
        this.linkAssistant = linkAssistant;
    }

    public AncestorPageCriterion( LinkAssistant linkAssistant, Page ancestorPage ) {
        this( linkAssistant, ancestorPage.getId() );
    }

    public boolean matches( Object object ) {
        Page parent = null;
        if ( object instanceof PageLink ) {
            Page page = (Page) ( (PageLink) object ).getDestinationContent();
            parent = page.getParent();
        }

        if ( object instanceof Page ) {
            parent = ( (Page) object ).getParent();
        }

        if ( parent != null ) {
            while ( parent != null ) {
                if ( ancestorPageId == parent.getId() )
                    return true;
                parent = parent.getParent();
            }
        }

        return false;
    }

    public long getAncestorPageId() {
        return ancestorPageId;
    }
}
