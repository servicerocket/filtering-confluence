/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.user;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.filtering.core.criteria.NotCriterion;

import java.util.regex.Pattern;

/**
 * Checks if the object is a User and if their name matches the one specified.
 * 
 * <p>
 * <b>Note:</b> This checks their 'user id', not their 'full name'.
 * 
 * @author David Peterson
 */
public class UserNameCriterion implements Criterion {
    public static class Interpreter implements CriterionInterpreter {
        public static final String SELF = "@self";

        public static final String ANONYMOUS = "@anonymous";

        public static final String AUTHENTICATED = "@authenticated";

        public Criterion createCriterion( String value ) {
            if ( SELF.equals( value ) ) {
                return new UserCriterion( AuthenticatedUserThreadLocal.getUser() );
            } else if ( ANONYMOUS.equals( value ) ) {
                return new AnonymousUserCriterion();
            } else if ( AUTHENTICATED.equals( value ) ) {
                return new NotCriterion( new AnonymousUserCriterion() );
            } else if ( value.indexOf( '*' ) >= 0 ) {
                // Convert the simple pattern match to a real regexp.
                value = value.replaceAll( "\\.", "\\." ).replaceAll( "\\*", ".*" );
                return new UserNameCriterion( value );
            } else {
                return new UserCriterion( value );
            }
        }
    }

    private Pattern namePattern;

    /**
     * Constructs a new criterion.
     * 
     * @param namePattern
     *            The Regular expression to match.
     */
    public UserNameCriterion( String namePattern ) {
        this.namePattern = Pattern.compile( namePattern );
    }

    public boolean matches( Object object ) {
        String username = null;
        if ( object instanceof User )
            username = ( ( User ) object ).getName();
        else if ( object instanceof String )
            username = ( String ) object;

        return username != null && namePattern.matcher( username ).matches();
    }

    @Override
    public String toString() {
        return "{username: " + namePattern + "}";
    }

}
