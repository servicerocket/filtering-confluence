package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.spring.container.ContainerManager;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;

public class SpaceThemeCriterion extends AbstractSpaceCriterion {

    public static class Interpreter implements CriterionInterpreter {

        public static final String GLOBAL_THEME = "@global";

        public Interpreter() {
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            if ( GLOBAL_THEME.equals( value ) ) {
                ThemeManager themeManager = ( ThemeManager ) ContainerManager.getComponent( "themeManager" );
                value = themeManager.getGlobalThemeKey();
            }
            return new SpaceThemeCriterion( value );
        }

    }

    private ThemeManager themeManager;

    private String themeKey;

    public SpaceThemeCriterion( String themeKey ) {
        this.themeKey = themeKey;
    }

    @Override
    protected boolean matchesSpace( Space space ) {
        String spaceThemeKey = themeManager.getSpaceThemeKey( space.getKey() );
        return themeKey.equals( spaceThemeKey );
    }

    @Override
    protected boolean matchesNullSpace() {
        String globalThemeKey = themeManager.getGlobalThemeKey();
        return themeKey.equals( globalThemeKey );
    }

    public void setThemeManager( ThemeManager themeManager ) {
        this.themeManager = themeManager;
    }

    public String getThemeKey() {
        return themeKey;
    }

    @Override
    public String toString() {
        return "{space theme: " + themeKey + "}";
    }
}
