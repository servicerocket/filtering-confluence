/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.criteria.content;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;

import java.util.List;

/**
 * This criterion will return <code>true</code> in two cases: <p/> 1. The
 * object being matched implements SpaceContentEntityObject and its space key
 * matches any of those specified in the constructor. 2. The object being
 * matched is a Space and it's space key matches any of those specified in the
 * constructor.
 * 
 * @author David Peterson
 */
public class SpaceKeyCriterion extends AbstractSpaceCriterion {

    public static class Interpreter implements CriterionInterpreter {
        /**
         * The key for 'current space' - "@self".
         */
        public static final String SELF_SPACE = "@self";

        private Space self;

        public Interpreter() {
            this( null );
        }

        public Interpreter( Space self ) {
            this.self = self;
        }

        /**
         * Returns the space which will be used for {@link #SELF_SPACE}
         * references.
         * 
         * @return The space for "@self".
         */
        public Space getSelf() {
            return self;
        }

        public Criterion createCriterion( String value ) throws CriteriaException {
            if ( SELF_SPACE.equals( value ) ) {
                // Set the space key to the current space.
                if ( self != null )
                    value = self.getKey();
                else
                    throw new CriteriaException( SELF_SPACE + " cannot be used in the current context." );
            }

            return new SpaceKeyCriterion( value );
        }
    }

    /**
     * All spaces, both personal and otherwise.
     */
    public static final String ALL_SPACES = "@all";

    /**
     * All non-personal spaces.
     */
    public static final String GLOBAL_SPACES = "@global";

    /**
     * All personal spaces.
     */
    public static final String PERSONAL_SPACES = "@personal";

    /**
     * All favourite spaces.
     */
    public static final String FAVOURITE_SPACES = "@favourite";

    public static final String FAVORITE_SPACES = "@favorite";

    private String spaceKey;

    protected LabelManager labelManager;

    public SpaceKeyCriterion( String spaceKey ) {
        this.spaceKey = spaceKey;
    }

    @Override
    protected boolean matchesSpace( Space space ) {
        if ( ALL_SPACES.equals( spaceKey ) )
            return true;
        else if ( GLOBAL_SPACES.equals( spaceKey ) )
            return space.isGlobal();
        else if ( PERSONAL_SPACES.equals( spaceKey ) )
            return space.isPersonal();
        else if ( FAVOURITE_SPACES.equals( spaceKey ) || FAVORITE_SPACES.equals( spaceKey ) ) {
            User user = AuthenticatedUserThreadLocal.getUser();
            if ( user != null ) {
                List<Space> favouriteSpaces = labelManager.getFavouriteSpaces( user.getName() );
                return favouriteSpaces.contains( space );
            }
            return false;
        }
        return spaceKey.equalsIgnoreCase( space.getKey() );
    }

    /**
     * @return The read-only set of keys this criterion matches.
     */
    public String getSpaceKey() {
        return spaceKey;
    }

    public void setLabelManager( LabelManager labelManager ) {
        this.labelManager = labelManager;
    }

    @Override
    public String toString() {
        return "{space key: " + spaceKey + "}";
    }
}
