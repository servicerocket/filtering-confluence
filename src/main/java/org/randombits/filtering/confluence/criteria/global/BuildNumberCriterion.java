package org.randombits.filtering.confluence.criteria.global;

import com.atlassian.confluence.util.GeneralUtil;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;

public class BuildNumberCriterion implements Criterion {
    private static final int UNBOUNDED_VALUE = -1;

    /**
     * This will interpret a string parameter value in the following ways:
     * 
     * <ol>
     * <li>A single number. E.g. "950" will set the min and max value to 950.</li>
     * <li>A range. E.g. "800-899" will set the min to 800 and the max to 899.</li>
     * <li>A wildcard value. E.g. "*-900" will allow any value up to and
     * including 900. "900-*" will include any value 900 or greater.</li>
     * </ol>
     * 
     * <p>
     * All values must be positive.
     */
    public static class Interpreter implements CriterionInterpreter {
        private static final String UNBOUNDED_STRING = "*";

        public Criterion createCriterion( String value ) throws CriteriaException {
            String[] range = value.trim().split( "\\S\\-\\S" );

            if ( range.length == 0 )
                throw new CriteriaException( "The value must have at least one build number: " + value );
            else if ( range.length > 2 )
                throw new CriteriaException( "The value can have at most a 'from' and 'to' value: " + value );

            int minValue = getValue( range[0] );
            int maxValue = range.length == 2 ? getValue( range[1] ) : minValue;

            return new BuildNumberCriterion( minValue, maxValue );
        }

        private int getValue( String value ) throws CriteriaException {
            try {
                if ( UNBOUNDED_STRING.equals( value ) )
                    return UNBOUNDED_VALUE;
                return Integer.parseInt( value );
            } catch ( NumberFormatException e ) {
                throw new CriteriaException( "Invalid number value: " + value );
            }
        }

    }

    private int minValue = UNBOUNDED_VALUE;

    private int maxValue = UNBOUNDED_VALUE;

    public BuildNumberCriterion( int minValue, int maxValue ) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public boolean matches( Object object ) {
        String buildNumber = GeneralUtil.getBuildNumber();
        int value = UNBOUNDED_VALUE;
        try {
            Integer.parseInt( buildNumber );
        } catch ( NumberFormatException e ) {
            return false;
        } catch ( NullPointerException e ) {
            return false;
        }

        if ( minValue >= 0 && minValue > value )
            return false;

        if ( maxValue >= 0 && maxValue < value )
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "{build number; min: " + ( minValue == UNBOUNDED_VALUE ? "*" : minValue ) + "; max: "
                + ( maxValue == UNBOUNDED_VALUE ? "*" : maxValue ) + "}";
    }

}
