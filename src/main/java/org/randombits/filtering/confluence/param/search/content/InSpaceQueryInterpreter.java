package org.randombits.filtering.confluence.param.search.content;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.search.service.SpaceCategoryEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.AllQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.SpaceCategoryQuery;

import org.apache.commons.lang.StringUtils;
import org.randombits.filtering.confluence.param.search.BaseMultipleSearchQueryInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String value as a {@link InSpaceQuery}.
 */
public class InSpaceQueryInterpreter extends AbstractParameterInterpreter<SearchQuery> {

    public static class Multiple extends BaseMultipleSearchQueryInterpreter<InSpaceQueryInterpreter> {

        public Multiple() {
            super( new InSpaceQueryInterpreter() );
        }
    }

    @Override
    public SearchQuery interpret( String value, ParameterContext context ) throws InterpretationException {
       	if ( value.startsWith( "@" ) ) {
            if ( value.equals( "@self" ) ) {
                ConversionContext ctx = context.get( ConversionContext.class, null );
                if ( ctx != null ) {
                    String spaceKey = ctx.getSpaceKey();
                    return new InSpaceQuery( spaceKey );
                }
            }
            SpaceCategoryEnum category;
            if ( value.equals( "@personal" ) )
                category = SpaceCategoryEnum.PERSONAL;
            else if ( value.equals( "@global" ) )
                category = SpaceCategoryEnum.GLOBAL;
            else if ( value.equals( "@favorite" ) || value.equals( "@favourite" ) )
                category = SpaceCategoryEnum.FAVOURITES;
            else if ( value.equals( "@all" ) )
                return AllQuery.getInstance();
            else
                throw new InterpretationException( ( new StringBuilder() ).append( "'" ).append( value ).append( "' is not a recognized space category" ).toString() );
            return new SpaceCategoryQuery( category );
        } else {
            return new InSpaceQuery( value );
        }
    }
}
