/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.criteria;

import java.util.Collection;
import java.util.List;

import org.randombits.filtering.confluence.param.ListQueryParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.filtering.confluence.param.criteria.content.ContentScopeParameter;
import org.randombits.filtering.confluence.param.criteria.content.ContentTypeParameter;
import org.randombits.filtering.confluence.param.criteria.content.LabelParameter;
import org.randombits.filtering.confluence.param.criteria.content.SpaceKeyParameter;
import org.randombits.filtering.core.criteria.Criteria;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.GroupCriteria;
import org.randombits.filtering.core.criteria.SourceCriterion;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroInfo;
import org.randombits.support.core.convert.ConversionAssistant;

import com.atlassian.confluence.core.ListQuery;

/**
 * A support class for grouping CriterionParameters together and producing a
 * single Criteria group for them.
 *
 * @author David Peterson
 */
public class CriterionParameterGroup {

    private List<CriterionParameter> params;

    public CriterionParameterGroup() {
        params = new java.util.ArrayList<CriterionParameter>();
    }

    public void addParameter( CriterionParameter parameter ) {
        params.add( parameter );
    }

    public Criteria createCriteria( MacroInfo info ) throws ParameterException {
        return createCriteria( info, true );
    }

    public Criteria createCriteria( MacroInfo info, boolean matchAll ) throws ParameterException {
        Criteria criteria = new GroupCriteria( matchAll );
        return addCriteria( criteria, info );
    }

    /**
     * Adds the criteria from this group to the specified criteria.
     *
     * @param criteria The criteria to add.
     * @param info     The Macro info.
     * @return
     */
    public Criteria addCriteria( Criteria criteria, MacroInfo info ) throws ParameterException {
        for ( CriterionParameter parameter : params ) {
            Criterion criterion = parameter.createCriterion( info );
            if ( criterion != null )
                criteria.addCriterion( criterion );
        }

        return criteria;
    }

    /**
     * Checks the criteria to see if they support {@link SourceCriterion} and
     * returns the set of values that match all the provided criteria. If an
     * <code>null</code> is returned, some of the criteria do not support
     * finding values, so the caller must apply the criteria themselves.
     *
     * @param info     The macro info.
     * @param matchAll If true, all criteria from all parameters must match.
     *                 Otherwise, any may match.
     * @return the matching values.
     * @throws ParameterException if there were any issues with the parameters in the group.
     */
    public Collection<?> getMatchingValues( MacroInfo info, boolean matchAll ) throws ParameterException {
        Criteria criteria = createCriteria( info, matchAll );
        if ( criteria instanceof SourceCriterion<?> ) {
            SourceCriterion<?> source = (SourceCriterion<?>) criteria;
            return source.getMatchingValues();
        }
        return null;
    }

    /**
     * Prepares the {@link com.atlassian.confluence.core.ListQuery} by passing it into all parameters that
     * implement the {@link ListQueryParameter} interface.
     *
     * @param query The query.
     * @param info  The macro info.
     * @throws ParameterException if there is a problem.
     */
    public void prepareListQuery( ListQuery query, MacroInfo info ) throws ParameterException {
        for ( CriterionParameter parameter : params ) {
            if ( parameter instanceof ListQueryParameter ) {
                ( (ListQueryParameter<?>) parameter ).prepareListQuery( query, info );
            }
        }
    }

    public static CriterionParameterGroup standardPageFilters( LinkAssistant linkAssistant, ConversionAssistant conversionAssistant ) {
        CriterionParameterGroup params = new CriterionParameterGroup();
        params.addParameter( new ContentTypeParameter() );
        params.addParameter( new LabelParameter() );
        params.addParameter( new ContentScopeParameter( conversionAssistant, linkAssistant ) );
        params.addParameter( new SpaceKeyParameter() );

        return params;
    }
}
