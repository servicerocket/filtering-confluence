package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import org.randombits.filtering.confluence.criteria.content.AncestorPageCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter as a single {@link AncestorPageCriterion}.
 */
public class AncestorPageCriterionInterpreter extends AbstractParameterInterpreter<AncestorPageCriterion> {

    /**
     * Instances of this class will support having multiple "+/-" values of {@link AncestorPageCriterionInterpreter}.
     */
    public static class Multiple extends BaseMultipleCriterionInterpreter<AncestorPageCriterionInterpreter> {

        public Multiple( LinkAssistant linkAssistant ) {
            super( new AncestorPageCriterionInterpreter( linkAssistant ) );
        }
    }

    /**
     * The key for 'current content' - "@self".
     */
    public static final String SELF = "@self";

    public static final String PARENT = "@parent";

    private final LinkAssistant linkAssistant;

    public AncestorPageCriterionInterpreter( LinkAssistant linkAssistant ) {
        this.linkAssistant = linkAssistant;
    }

    private Page asPage( ConfluenceEntityObject content, String value ) throws InterpretationException {
        if ( content instanceof Page )
            return (Page) content;
        else
            throw new InterpretationException( "'" + value + "' could not be found in this context." );
    }

    @Override
    public AncestorPageCriterion interpret( String inputValue, ParameterContext parameterContext ) throws InterpretationException {
        ContentEntityObject self = parameterContext.get( ContentEntityObject.class, null );

        Page page;
        if ( SELF.equals( inputValue ) ) {
            // Set the page to the 'self'.
            page = asPage( self, inputValue );
        } else if ( PARENT.equals( inputValue ) ) {
            page = asPage( self, inputValue ).getParent();
        } else {
            ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( new DefaultConversionContext( self.toPageContext() ),
                    inputValue );
            page = asPage( entity, inputValue );
        }

        if ( page != null )
            throw new InterpretationException( "'" + inputValue + "' could not be found in this context." );

        return new AncestorPageCriterion( linkAssistant, page );
    }
}
