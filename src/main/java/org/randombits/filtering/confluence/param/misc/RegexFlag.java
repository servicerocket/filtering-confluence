package org.randombits.filtering.confluence.param.misc;

import java.util.EnumSet;
import java.util.regex.Pattern;

/**
* Created by IntelliJ IDEA.
* User: david
* Date: 28/01/12
* Time: 3:39 PM
* To change this template use File | Settings | File Templates.
*/
public enum RegexFlag {
    CANON_EQ( Pattern.CANON_EQ ),
    CASE_INSENSITIVE( Pattern.CASE_INSENSITIVE ),
    COMMENTS( Pattern.COMMENTS ),
    DOTALL( Pattern.DOTALL ),
    LITERAL( Pattern.LITERAL ),
    MULTILINE( Pattern.MULTILINE ),
    UNICODE_CASE( Pattern.UNICODE_CASE ),
    UNIX_LINES( Pattern.UNIX_LINES );

    private final int value;

    RegexFlag( int value ) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static int combine( EnumSet<RegexFlag> flags ) {
        int value = 0;
        for ( RegexFlag flag : flags ) {
            value += flag.value;
        }
        return value;
    }

    public static EnumSet<RegexFlag> split( int flags ) {
        EnumSet<RegexFlag> split = EnumSet.noneOf( RegexFlag.class );
        for ( RegexFlag flag : RegexFlag.values() ) {
            if ( ( flag.value | flags ) == flag.value )
                split.add( flag );
        }
        return split;
    }

    public static RegexFlag find( int flagId ) {
        for ( RegexFlag flag : RegexFlag.values() ) {
            if ( flag.value == flagId )
                return flag;
        }
        return null;
    }
}
