package org.randombits.filtering.confluence.param.search.content;

import com.atlassian.confluence.search.v2.query.LabelQuery;
import org.randombits.filtering.confluence.param.search.BaseMultipleSearchQueryInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link LabelQuery}.
 */
public class LabelQueryInterpreter extends AbstractParameterInterpreter<LabelQuery> {

    public static class Multiple extends BaseMultipleSearchQueryInterpreter<LabelQueryInterpreter> {

        public Multiple() {
            super( new LabelQueryInterpreter() );
        }
    }

    @Override
    public LabelQuery interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return new LabelQuery( inputValue );
    }
}
