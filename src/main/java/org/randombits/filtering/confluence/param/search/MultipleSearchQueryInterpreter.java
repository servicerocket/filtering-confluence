package org.randombits.filtering.confluence.param.search;

import com.atlassian.confluence.search.v2.SearchQuery;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 *
 */
public class MultipleSearchQueryInterpreter extends AbstractMultipleSearchQueryInterpreter {

    private final ParameterAssistant parameterAssistant;

    public MultipleSearchQueryInterpreter( ParameterAssistant parameterAssistant ) {
        this.parameterAssistant = parameterAssistant;
    }

    @Override
    public SearchQuery createInstance( String inputValue, ParameterContext context ) throws InterpretationException {
        MultipleSearchQueryValues searchQueryValues = context.getAnnotation( MultipleSearchQueryValues.class );
        if ( searchQueryValues == null )
            throw new InterpretationException( "Please provide a @MultipleSearchQueryValues annotation on this parameter." );

        ParameterInterpreter<? extends SearchQuery> interpreter = parameterAssistant.findParameterInterpreter( searchQueryValues.value() );

        if ( interpreter == null )
            throw new InterpretationException( "Ensure the following interpreter has a <parameter-interpreter> module in 'atlassian-plugin.xml': " + searchQueryValues.value().getName() );

        return interpreter.interpret( inputValue, context, SearchQuery.class );
    }
}
