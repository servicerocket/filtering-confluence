package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import org.randombits.filtering.confluence.criteria.content.SpaceKeyCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link SpaceKeyCriterion}. If the string value is
 * {@link #SELF_SPACE} ('\@self'), it will look for the current space key via the {@link ConversionContext}
 * in the {@link ParameterContext}.
 */
public class SpaceKeyCriterionInterpreter extends AbstractParameterInterpreter<SpaceKeyCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<SpaceKeyCriterionInterpreter> {

        public Multiple() {
            super( new SpaceKeyCriterionInterpreter() );
        }
    }

    /**
     * The key for 'current space' - "@self".
     */
    public static final String SELF_SPACE = "@self";

    @Override
    public SpaceKeyCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        String spaceKey = getSpaceKey( inputValue, context );
        return new SpaceKeyCriterion( spaceKey );
    }

    private String getSpaceKey( String inputValue, ParameterContext context ) {
        if ( SELF_SPACE.equals( inputValue ) ) {
            // Find the current space
            ConversionContext cc = context.get( ConversionContext.class, null );
            if ( cc != null )
                return cc.getSpaceKey();
            else
                return null;
        }
        return inputValue;
    }
}
