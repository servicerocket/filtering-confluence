package org.randombits.filtering.confluence.param.format;

import com.atlassian.renderer.v2.components.HtmlEscaper;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets separator parameters.
 */
public class SeparatorInterpreter extends AbstractParameterInterpreter<Separator> {

    private static final String CUSTOM = "custom";

    @Override
    public Separator interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        String customValue = context.get( String.class, null );

        if ( customValue != null || CUSTOM.equalsIgnoreCase( inputValue ) ) {
            return new Separator( "", HtmlEscaper.escapeAll( customValue, true ), "" );
        } else {
            return Separator.parse( inputValue );
        }
    }
}
