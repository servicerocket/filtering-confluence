package org.randombits.filtering.confluence.param.criteria;

import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.support.core.param.Context;
import org.randombits.support.core.param.ParameterInterpreter;

import java.lang.annotation.*;

/**
 * Provides details about the {@link Criterion} values that are used by {@link MultipleCriterionInterpreter}.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface MultipleCriterionValues {

    /**
     * The {@link ParameterInterpreter}, which must interpret into some form of {@link Criterion}.
     *
     * @return
     */
    Class<? extends ParameterInterpreter<? extends Criterion>> value();
}
