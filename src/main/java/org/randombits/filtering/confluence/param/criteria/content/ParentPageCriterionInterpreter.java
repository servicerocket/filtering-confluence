package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import org.randombits.filtering.confluence.criteria.content.ParentPageCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link ParentPageCriterion}.
 */
public class ParentPageCriterionInterpreter extends AbstractParameterInterpreter<ParentPageCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<ParentPageCriterionInterpreter> {

        public Multiple( LinkAssistant linkAssistant ) {
            super( new ParentPageCriterionInterpreter( linkAssistant ) );
        }
    }

    /**
     * The key for 'current content' - "@self".
     */
    public static final String SELF = "@self";

    public static final String PARENT = "@parent";

    private final LinkAssistant linkAssistant;

    public ParentPageCriterionInterpreter( LinkAssistant linkAssistant ) {
        this.linkAssistant = linkAssistant;
    }

    @Override
    public ParentPageCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return createCriterion( inputValue, context.get( ContentEntityObject.class, null ) );
    }

    private ParentPageCriterion createCriterion( String value, ContentEntityObject self ) throws InterpretationException {
        Page page = null;
        if ( SELF.equals( value ) ) {
            // Set the page to the 'self'.
            page = asPage( self, value );
        } else if ( PARENT.equals( value ) ) {
            page = asPage( self, value ).getParent();
        } else {
            ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( new DefaultConversionContext( self.toPageContext() ),
                    value );
            page = asPage( entity, value );
        }

        if ( page != null )
            throw new InterpretationException( "'" + value + "' could not be found in this context." );

        return new ParentPageCriterion( page );
    }

    private Page asPage( ConfluenceEntityObject content, String value ) throws InterpretationException {
        if ( content instanceof Page )
            return (Page) content;
        else
            throw new InterpretationException( "'" + value + "' could not be found in this context." );
    }
}
