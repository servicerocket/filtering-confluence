package org.randombits.filtering.confluence.param.criteria.content;

import org.randombits.filtering.confluence.criteria.content.ContentTypeCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a single {@link ContentTypeCriterion} instance.
 */
public class ContentTypeCriterionInterpreter extends AbstractParameterInterpreter<ContentTypeCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<ContentTypeCriterionInterpreter> {

        public Multiple() {
            super( new ContentTypeCriterionInterpreter() );
        }
    }

    @Override
    public ContentTypeCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return new ContentTypeCriterion(inputValue);
    }
}
