package org.randombits.filtering.confluence.param.content;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

/**
 * Interprets a parameter string as a {@link ConfluenceEntityObject}. It uses wiki markup conventions,
 * as well as '@self', '@parent' and '@home'.
 *
 * @see AbstractConfluenceEntityObjectInterpreter
 */
public class ConfluenceEntityObjectInterpreter extends AbstractConfluenceEntityObjectInterpreter<ConfluenceEntityObject> {

    public ConfluenceEntityObjectInterpreter( LinkAssistant linkAssistant, EnvironmentAssistant environmentAssistant ) {
        super( linkAssistant, environmentAssistant );
    }
}
