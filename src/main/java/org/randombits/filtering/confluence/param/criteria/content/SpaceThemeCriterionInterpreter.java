package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.themes.ThemeManager;
import org.randombits.filtering.confluence.criteria.content.SpaceThemeCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link SpaceThemeCriterion} instance.
 */
public class SpaceThemeCriterionInterpreter extends AbstractParameterInterpreter<SpaceThemeCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<SpaceThemeCriterionInterpreter> {

        public Multiple( ThemeManager themeManager ) {
            super( new SpaceThemeCriterionInterpreter( themeManager ) );
        }
    }

    public static final String GLOBAL_THEME = "@global";

    private final ThemeManager themeManager;

    public SpaceThemeCriterionInterpreter( ThemeManager themeManager ) {
        this.themeManager = themeManager;
    }

    @Override
    public SpaceThemeCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        if ( GLOBAL_THEME.equals( inputValue ) ) {
            inputValue = themeManager.getGlobalThemeKey();
        }
        return new SpaceThemeCriterion( inputValue );

    }
}
