package org.randombits.filtering.confluence.param.criteria.global;

import org.randombits.filtering.confluence.criteria.global.BuildNumberCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link BuildNumberCriterion}. It allows for setting a build number range
 * such as "100-199". The value "*" will allow any build number.
 */
public class BuildNumberCriterionInterpreter extends AbstractParameterInterpreter<BuildNumberCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<BuildNumberCriterionInterpreter> {

        public Multiple() {
            super( new BuildNumberCriterionInterpreter() );
        }
    }

    private static final String UNBOUNDED_STRING = "*";

    private static final int UNBOUNDED_VALUE = -1;

    public BuildNumberCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        String[] range = inputValue.trim().split( "\\S\\-\\S" );

        if ( range.length == 0 )
            throw new InterpretationException( "The value must have at least one build number: " + inputValue );
        else if ( range.length > 2 )
            throw new InterpretationException( "The value can have at most a 'from' and 'to' value: " + inputValue );

        int minValue = getValue( range[0] );
        int maxValue = range.length == 2 ? getValue( range[1] ) : minValue;

        return new BuildNumberCriterion( minValue, maxValue );
    }

    private int getValue( String value ) throws InterpretationException {
        try {
            if ( UNBOUNDED_STRING.equals( value ) )
                return UNBOUNDED_VALUE;
            return Integer.parseInt( value );
        } catch ( NumberFormatException e ) {
            throw new InterpretationException( "Invalid number value: " + value );
        }
    }

}
