package org.randombits.filtering.confluence.param.content;

import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

/**
 * Interprets a parameter String as a {@link ContentEntityObject}, using wiki markup links and
 * '@self', '@parent' and '@home'.
 *
 * @see AbstractConfluenceEntityObjectInterpreter
 */
public class ContentEntityObjectInterpreter extends AbstractConfluenceEntityObjectInterpreter<ContentEntityObject> {

    public ContentEntityObjectInterpreter( LinkAssistant linkAssistant, EnvironmentAssistant environmentAssistant ) {
        super( linkAssistant, environmentAssistant );
    }
}
