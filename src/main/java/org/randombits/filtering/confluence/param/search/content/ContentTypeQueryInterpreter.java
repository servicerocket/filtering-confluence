package org.randombits.filtering.confluence.param.search.content;

import java.util.HashMap;
import java.util.Map;

import org.randombits.filtering.confluence.param.search.BaseMultipleSearchQueryInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;

/**
 * Interprets a parameter String as a {@link ContentTypeQuery}.
 */
public class ContentTypeQueryInterpreter extends AbstractParameterInterpreter<ContentTypeQuery> {

    public static class Multiple extends BaseMultipleSearchQueryInterpreter<ContentTypeQueryInterpreter> {

        public Multiple() {
            super( new ContentTypeQueryInterpreter() );
        }
    }

    private static final Map<String,String> synonym ;
    
    static{
    	synonym = new HashMap<String, String>(); 
    	synonym.put("news", ContentTypeEnum.BLOG.getRepresentation());
    	synonym.put("blogposts", ContentTypeEnum.BLOG.getRepresentation()); 
    	synonym.put("spacedescription", ContentTypeEnum.SPACE_DESCRIPTION.getRepresentation()) ;
    	synonym.put("attachments", ContentTypeEnum.ATTACHMENT.getRepresentation() ); 
    	synonym.put("spaces", ContentTypeEnum.SPACE.getRepresentation() );
    }
    
    @Override
    public ContentTypeQuery interpret( String inputValue, ParameterContext context ) throws InterpretationException {
    	return new ContentTypeQuery( findContentTypeEnum( synonym.get(inputValue)==null? inputValue : synonym.get(inputValue) ) );
    }

    private ContentTypeEnum findContentTypeEnum( String inputValue ) {
        return ContentTypeEnum.getByRepresentation( inputValue );
    }
}
