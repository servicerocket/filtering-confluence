package org.randombits.filtering.confluence.param.search;

import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import org.randombits.support.core.param.AbstractMultipleParameterInterpreter;
import org.randombits.support.core.param.ParameterContext;

import java.util.Set;

/**
 * An abstract base class for {@link org.randombits.support.core.param.ParameterInterpreter}s
 * that can be converted into {@link org.randombits.filtering.core.criteria.Criterion} or {@link org.randombits.filtering.core.criteria.Criteria}. It provides support for creating
 * a complex {@link org.randombits.filtering.core.criteria.Criterion} that allows for '+/-' flagging and groups using parentheses. This class
 * handles all the group parsing, subclasses simply need to provide the implementation
 * {@link #createInstance(String, org.randombits.support.core.param.ParameterContext)} which should
 * create a single instance of the {@link org.randombits.filtering.core.criteria.Criterion}.
 */
public abstract class AbstractMultipleSearchQueryInterpreter extends AbstractMultipleParameterInterpreter<SearchQuery> {

    @Override
    public SearchQuery createGroup( Set<? extends SearchQuery> must, Set<? extends SearchQuery> should, Set<? extends SearchQuery> mustNot,
                                    ParameterContext context ) {
        return new BooleanQuery( must, should, mustNot );
    }
}
