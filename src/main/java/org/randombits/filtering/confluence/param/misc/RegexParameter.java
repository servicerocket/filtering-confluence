/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.misc;

import org.randombits.filtering.confluence.param.BaseParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.support.confluence.MacroInfo;

import java.util.EnumSet;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Handles a parameter which is a regular expression. This requires some extra
 * work because macros don't allow the '|' character.
 *
 * @author David Peterson
 */
public class RegexParameter extends BaseParameter<Pattern> {

    private final EnumSet<RegexFlag> flags;

    public RegexParameter( List<String> names, String defaultValue ) {
        super( names, defaultValue );
        flags = EnumSet.noneOf( RegexFlag.class );
    }

    public RegexParameter( String[] names, String defaultValue ) {
        this( names, defaultValue, EnumSet.noneOf( RegexFlag.class ) );
    }

    public RegexParameter( String[] names, String defaultValue, EnumSet<RegexFlag> flags ) {
        super( names, defaultValue );
        this.flags = flags;
    }

    public EnumSet<RegexFlag> getFlags() {
        return flags;
    }

    @Override
    protected Pattern findObject( String paramValue, MacroInfo info ) throws ParameterException {
        Pattern pattern = null;
        if ( paramValue != null ) {
            paramValue = paramValue.replaceAll( ",\\s*", "|" );
            pattern = Pattern.compile( paramValue, RegexFlag.combine( flags ) );
        }
        return pattern;
    }
}
