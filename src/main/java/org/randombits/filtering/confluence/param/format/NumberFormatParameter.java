/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.format;

import org.randombits.filtering.confluence.param.BaseParameter;
import org.randombits.support.confluence.MacroInfo;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Creates a {@link java.text.NumberFormat} from the specified format parameter.
 *
 * @author David Peterson
 */
public class NumberFormatParameter extends BaseParameter<NumberFormat> {

    private static final String FORMAT_PARAM = "format";

    private static final String STANDARD_FORMAT = "standard";

    private static final String NUMBER_FORMAT = "number";

    private static final String INTEGER_FORMAT = "integer";

    private static final String PERCENT_FORMAT = "percent";

    private static final String CURRENCY_FORMAT = "currency";

    public NumberFormatParameter() {
        this( null );
    }

    public NumberFormatParameter( String defaultValue ) {
        super( new String[]{FORMAT_PARAM}, defaultValue );
    }

    @Override
    protected NumberFormat findObject( String paramValue, MacroInfo info ) {
        return getNumberFormat( paramValue );
    }

    private NumberFormat getNumberFormat( String format ) {
        if ( STANDARD_FORMAT.equals( format ) )
            return NumberFormat.getInstance();
        else if ( CURRENCY_FORMAT.equals( format ) )
            return NumberFormat.getCurrencyInstance();
        else if ( INTEGER_FORMAT.equals( format ) )
            return NumberFormat.getIntegerInstance();
        else if ( PERCENT_FORMAT.equals( format ) )
            return NumberFormat.getPercentInstance();
        else if ( NUMBER_FORMAT.equals( format ) )
            return NumberFormat.getNumberInstance();
        else if ( format != null )
            return new DecimalFormat( format );

        return null;
    }

}
