package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import org.randombits.filtering.confluence.criteria.content.ContentScopeCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.convert.ConversionAssistant;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.util.EnumSet;

/**
 * Interprets a parameter value as a {@link ContentScopeCriterion}.
 */
public class ContentScopeCriterionInterpreter extends AbstractParameterInterpreter<ContentScopeCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<ContentScopeCriterionInterpreter> {

        public Multiple( LinkAssistant linkAssistant, ConversionAssistant conversionAssistant ) {
            super( new ContentScopeCriterionInterpreter( linkAssistant, conversionAssistant ) );
        }
    }

    /**
     * This marks the delimitation between the content and the scope.
     */
    public static final char SCOPE_DELIMITER = '>';

    private final LinkAssistant linkAssistant; 
    private final ConversionAssistant conversionAssistant; 

    public ContentScopeCriterionInterpreter( LinkAssistant linkAssistant, ConversionAssistant conversionAssistant) {
        this.linkAssistant = linkAssistant;
        this.conversionAssistant = conversionAssistant; 
    }

    @Override
    public ContentScopeCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        int scopeIndex = inputValue.indexOf( SCOPE_DELIMITER );
        EnumSet<ContentScopeCriterion.Scope> scopeSet = EnumSet.noneOf( ContentScopeCriterion.Scope.class );

        if ( scopeIndex >= 0 ) {
            String scopeString = inputValue.substring( scopeIndex + 1 ).trim().toUpperCase();
            inputValue = inputValue.substring( 0, scopeIndex ).trim();

            if ( "ancestors".equalsIgnoreCase( scopeString ) ) {
                scopeSet.add( ContentScopeCriterion.Scope.ANCESTORS );
            } else if ( "children".equalsIgnoreCase( scopeString ) ) {
                scopeSet.add( ContentScopeCriterion.Scope.CHILDREN );
            } else if ( "descendents".equalsIgnoreCase( scopeString ) || "descendants".equalsIgnoreCase( scopeString ) ) {
                scopeSet.add( ContentScopeCriterion.Scope.DESCENDANTS );
            } else {
                throw new InterpretationException( "Unsupported scope: " + scopeString );
            }
        } else {
            scopeSet.add( ContentScopeCriterion.Scope.SELF );
        }

        ContentEntityObject currentContent = context.get( ContentEntityObject.class, null );

        ContentEntityObject targetContent = null;

        if ( "@self".equals( inputValue ) ) {
            targetContent = currentContent;
        } else if ( "@parent".equals( inputValue ) ) {
            if ( currentContent instanceof Page ) {
                targetContent = ( (Page) currentContent ).getParent();
            } else
                throw new InterpretationException( "@parent can only be used on pages." );
        } else if ( "@home".equals( inputValue ) ) {
            if ( currentContent instanceof SpaceContentEntityObject ) {
                Space space = ( (SpaceContentEntityObject) currentContent ).getSpace();
                if ( space != null )
                    targetContent = space.getHomePage();
            }
        } else {
            ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink(
                    new DefaultConversionContext( currentContent.toPageContext() ), inputValue );
            if ( entity instanceof ContentEntityObject )
                targetContent = (ContentEntityObject) entity;
        }

        if ( targetContent != null )
            return new ContentScopeCriterion(conversionAssistant, targetContent, scopeSet );
        else
            throw new InterpretationException( "Unable to locate the specified content: '" + inputValue + "'" );
    }
}
