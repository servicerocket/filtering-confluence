package org.randombits.filtering.confluence.param.search;

import com.atlassian.confluence.search.v2.SearchQuery;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 * Provides a base class that will create a 'multiple' {@link SearchQuery} parameter based
 * on the provided {@link org.randombits.support.core.param.ParameterInterpreter} which also supports {@link SearchQuery}.
 */
public class BaseMultipleSearchQueryInterpreter<I extends ParameterInterpreter<? extends SearchQuery>> extends AbstractMultipleSearchQueryInterpreter {

    private final I instanceInterpreter;

    public BaseMultipleSearchQueryInterpreter( I instanceInterpreter ) {
        this.instanceInterpreter = instanceInterpreter;
    }

    @Override
    public SearchQuery createInstance( String inputValue, ParameterContext context ) throws InterpretationException {
        return instanceInterpreter.interpret( inputValue, context, SearchQuery.class );
    }
}
