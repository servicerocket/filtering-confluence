/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.criteria;

import org.apache.commons.lang.StringUtils;
import org.randombits.filtering.confluence.param.BaseParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.filtering.core.criteria.CriteriaException;
import org.randombits.filtering.core.criteria.CriteriaParser;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.MacroInfo;

import java.util.List;

/**
 * An abstract base class for parameters which whose values are
 * {@link Criterion}.
 *
 * @author David Peterson
 */
public abstract class CriterionParameter extends BaseParameter<Criterion> {

    protected CriterionParameter( String[] names, String defaultValue ) {
        super( names, defaultValue );
    }

    protected CriterionParameter( List<String> names, String defaultValue ) {
        super( names, defaultValue );
    }

    /**
     * This method checks if the object value matches the specified criterion.
     * If the parameter is not defined and no criterion is generated,
     * <code>true</code> is returned.
     *
     * @param info  The macro info.
     * @param value The value to check.
     * @return The match value, or <code>true</code> if no criterion is
     *         created.
     * @throws ParameterException if there is a problem with the parameter.
     */
    public boolean matches( MacroInfo info, Object value ) throws ParameterException {
        return matches( info, value, true );
    }

    /**
     * This method checks if the object value matches the specified criterion.
     * If no criterion is created, the <code>defaultMatch</code> is returned
     * instead.
     *
     * @param info         The macro info.
     * @param value        The value to check.
     * @param defaultMatch The match value to return if no criterion is specified.
     * @return <code>true</code> or <code>false</code>.
     * @throws ParameterException if there is a problem with the parameter.
     */
    public boolean matches( MacroInfo info, Object value, boolean defaultMatch ) throws ParameterException {
        Criterion criterion = createCriterion( info );
        if ( criterion != null )
            return criterion.matches( value );

        return defaultMatch;
    }

    /**
     * Called to construct the individual criterion parser for this context.
     *
     * @param info The macro info.
     * @return The criterion parser.
     * @throws ParameterException if there is a problem.
     */
    protected abstract CriterionInterpreter createCriterionInterpreter( MacroInfo info ) throws ParameterException;

    /**
     * Called to construct the individual criterion parser for this context. This
     * is provided to give public access to the {@link #createCriterionInterpreter(MacroInfo)}
     * method.
     *
     * @param info The MacroInfo.
     * @return The {@link CriterionInterpreter}.
     * @throws ParameterException if there is a problem.
     */
    public CriterionInterpreter createInterpreter( MacroInfo info ) throws ParameterException {
        return createCriterionInterpreter( info );
    }

    public Criterion createCriterion( MacroInfo info ) throws ParameterException {
        return findValue( info );
    }

    @Override
    protected final Criterion findObject( String parameter, MacroInfo info ) throws ParameterException {
        return createCriterion( parameter, info );
    }

    protected Criterion createCriterion( String parameter, MacroInfo info ) throws ParameterException {
        if ( parameter != null )
            try {
                return new CriteriaParser().parse( parameter, createInterpreter( info ) );
            } catch ( CriteriaException e ) {
                String message = e.getLocalizedMessage();
                throw new ParameterException(
                        StringUtils.isBlank( message ) ? "Problem occurred while parsing the criteria parameter"
                                : message, e );
            }
        return null;
    }
}
