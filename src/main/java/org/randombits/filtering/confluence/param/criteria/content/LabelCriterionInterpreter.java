package org.randombits.filtering.confluence.param.criteria.content;

import org.randombits.filtering.confluence.criteria.content.LabelCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link LabelCriterion}.
 */
public class LabelCriterionInterpreter extends AbstractParameterInterpreter<LabelCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<LabelCriterionInterpreter> {

        public Multiple() {
            super( new LabelCriterionInterpreter() );
        }
    }

    @Override
    public LabelCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return new LabelCriterion( inputValue );
    }
}
