/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.sort;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.randombits.filtering.confluence.param.BaseParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.support.confluence.MacroInfo;
import org.randombits.support.confluence.sorting.*;

import java.util.*;

/**
 * Parses a parameter of the form "alias( [asc|desc])(, alias...)". Eg: <p/>
 * <p/>
 * <pre>
 * natural, creation desc
 * </pre>
 * <p/>
 * <p/>
 * <p>
 * "natural" and "creation" are two of the default comparator types. The
 * complete list follows:
 * </p>
 * <ul>
 * <li><i>natural</i>: Sorts based on the natural title order. Eg. "Page 2"
 * will be before "Page 10". See {@link NaturalTitleComparator}</li>
 * <li><i>exact</i>: Sorts based on the exact title order. Eg. "Page 2" will
 * be <i>after</i> "Page 10". See {@link ExactTitleComparator}</li>
 * <li><i>creation</i>: Sorts based on the creation date of the content. See
 * {@link CreationDateComparator}</li>
 * <li><i>modified</i>: Sorts based on the last modification date of the
 * content. See {@link ModificationDateComparator}</li>
 * </ul>
 * <p/>
 * <p>
 * "asc" indicates ascending order (eg. a-z), "desc" indicates descending order
 * (eg. z-a).
 * </p>
 *
 * @author David Peterson
 */
public class SortParameter<T> extends BaseParameter<Comparator<? super T>> {

    private static final String DESC = " desc";

    private static final String ASC = " asc";

    public static final String NATURAL_TITLE = "natural title";

    public static final String EXACT_TITLE = "exact title";

    public static final String CREATION_DATE = "creation date";

    public static final String MODIFICATION_DATE = "modification date";

    public static final String SPACE_KEY = "space key";

    public static final String SPACE_NAME = "space name";

    private static final Map<String, Comparator<Object>> STANDARD_SORTS = new HashMap<String, Comparator<Object>>();

    static {
        STANDARD_SORTS.put( NATURAL_TITLE, new NaturalTitleComparator<Object>() );
        STANDARD_SORTS.put( EXACT_TITLE, new ExactTitleComparator<Object>() );
        STANDARD_SORTS.put( CREATION_DATE, new CreationDateComparator<Object>() );
        STANDARD_SORTS.put( MODIFICATION_DATE, new ModificationDateComparator<Object>() );
        STANDARD_SORTS.put( SPACE_KEY, new SpaceKeyComparator<Object>() );
        STANDARD_SORTS.put( SPACE_NAME, new SpaceNameComparator<Object>() );
    }

    private Map<String, Comparator<? super T>> sorts;

    private List<Comparator<? super T>> defaults;

    public SortParameter( Comparator<? super T>[] defaults ) {
        this( defaults != null ? Arrays.asList( defaults ) : null );
    }

    public SortParameter( Collection<Comparator<? super T>> defaults ) {
        this( defaults, true );
    }

    public SortParameter( Comparator<? super T>[] defaults, boolean registerStandard ) {
        this( Arrays.asList( defaults ), registerStandard );
    }

    /**
     * Constructs a new Sort parameter. The Comparators in <code>defaults<code>
     * will be used if no comparators are specified in the provided parameter value.
     *
     * @param defaults         The collection of Comparators to use if none are specified in the parameter value.
     *                         The contents of the collection is used, not the collection itself.
     * @param registerStandard If <code>true</code>, register the standard comparators.
     */
    public SortParameter( Collection<Comparator<? super T>> defaults, boolean registerStandard ) {
        super( new String[]{"sort"}, null );
        sorts = new HashMap<String, Comparator<? super T>>();
        if ( registerStandard )
            sorts.putAll( STANDARD_SORTS );

        if ( defaults != null ) {
            this.defaults = new ArrayList<Comparator<? super T>>( defaults.size() );
            this.defaults.addAll( defaults );
        }
    }

    public void registerComparator( String name, Comparator<? super T> comparator ) {
        sorts.put( name, comparator );
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Comparator<? super T> findObject( String parameter, MacroInfo info )
            throws ParameterException {
        ComparatorChain chain = new ComparatorChain();
        if ( parameter != null ) {
            String[] params = parameter.toLowerCase().split( "[,;]" );

            Comparator<T> comparator;
            boolean descending;

            for ( int i = 0; i < params.length; i++ ) {
                descending = false;
                params[i] = params[i].trim();
                if ( params[i].endsWith( DESC ) ) {
                    descending = true;
                    params[i] = params[i].substring( 0, params[i].length() - DESC.length() );
                } else if ( params[i].endsWith( ASC ) ) {
                    params[i] = params[i].substring( 0, params[i].length() - ASC.length() );
                }

                comparator = (Comparator<T>) sorts.get( params[i] );
                if ( comparator == null )
                    throw new ParameterException( "Unsupported comparator: " + params[i] );

                if ( descending )
                    comparator = new ReverseComparator( comparator );

                chain.addComparator( comparator );
            }
        }

        if ( chain.size() == 0 ) {
            if ( defaults != null ) {
                for ( Comparator<? super T> c : defaults )
                    chain.addComparator( c );
            } else {
                return null;
            }
        }

        return chain;
    }

    /**
     * Creates a comparator based on the contents of the specified macro info.
     *
     * @param info The macro info.
     * @return The comparator.
     * @throws ParameterException if there is a problem processing the parameter.
     */
    public Comparator<? super T> createComparator( MacroInfo info ) throws ParameterException {
        return findValue( info );
    }

    /**
     * Creates a comparator based on the specified parameter value string.
     *
     * @param parameterValue The parameter value.
     * @return The comparator.
     * @throws ParameterException if there is a problem processing the parameter.
     */
    public Comparator<? super T> createComparator( String parameterValue ) throws ParameterException {
        return findObject( parameterValue, null );
    }
}
