/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.core.ListQuery;
import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.randombits.filtering.confluence.criteria.content.ContentTypeCriterion;
import org.randombits.filtering.confluence.param.ListQueryParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.MacroInfo;

import java.util.List;

/**
 * Handles the 'type(s)' parameter, checking the content type of the object in
 * question.
 *
 * @author David Peterson
 */
public class ContentTypeParameter extends CriterionParameter implements ListQueryParameter<Criterion> {

    public static final String[] NAMES = new String[]{"type", "types"};

    private static final List<String> TYPES = new java.util.ArrayList<String>();

    static {
        TYPES.add( Page.CONTENT_TYPE );
        TYPES.add( BlogPost.CONTENT_TYPE );
        TYPES.add( BlogPost.CONTENT_TYPE );
        TYPES.add( SpaceDescription.CONTENT_TYPE_SPACEDESC );
        TYPES.add( Comment.CONTENT_TYPE );
        TYPES.add( Attachment.CONTENT_TYPE );
        TYPES.add( Mail.CONTENT_TYPE );

        TYPES.add( ContentTypeCriterion.SPACE_TYPE );

        TYPES.add( ContentTypeCriterion.MAILTO_TYPE );
        TYPES.add( ContentTypeCriterion.URL_TYPE );
        TYPES.add( ContentTypeCriterion.UNRESOLVED_TYPE );
    }

    public ContentTypeParameter() {
        this( null );
    }

    public ContentTypeParameter( String defaultValue ) {
        super( NAMES, defaultValue );
    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new ContentTypeCriterion.Interpeter();
    }

    /**
     * Prepares the specified list query to apply the settings in this
     * parameter.
     *
     * @param listQuery The list query.
     * @param info      The macro info.
     * @throws ParameterException if there is a problem while applying the parameter settings.
     */
    public void prepareListQuery( ListQuery listQuery, MacroInfo info ) throws ParameterException {
        Criterion criterion = createCriterion( info );
        if ( criterion != null ) {
            List<String> types = new java.util.ArrayList<String>();

            for ( String type : TYPES ) {
                if ( criterion.matches( type ) )
                    types.add( type );
            }

            listQuery.setTypeList( types );
        }
    }
}
