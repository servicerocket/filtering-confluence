package org.randombits.filtering.confluence.param.content;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.renderer.PageContext;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.env.EnvironmentAssistant;

/**
 * Interprets a string as a wiki-style attachment link.
 */
public class AttachmentInterpreter extends AbstractParameterInterpreter<Attachment> {

    private final EnvironmentAssistant environmentAssistant;

    private final LinkAssistant linkAssistant;

    public AttachmentInterpreter( EnvironmentAssistant environmentAssistant, LinkAssistant linkAssistant ) {
        this.environmentAssistant = environmentAssistant;
        this.linkAssistant = linkAssistant;
    }

    @Override
    public Attachment interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        ConversionContext content = context.get( ConversionContext.class, null );
        if ( content == null )
            content = environmentAssistant.getValue( ConversionContext.class );

        if ( content == null )
            content = new DefaultConversionContext( new PageContext() );

        ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( content, inputValue );

        if ( entity instanceof Attachment )
            return (Attachment) entity;

        return null;
    }
}
