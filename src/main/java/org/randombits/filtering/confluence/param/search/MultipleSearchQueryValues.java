package org.randombits.filtering.confluence.param.search;

import com.atlassian.confluence.search.v2.SearchQuery;
import org.randombits.support.core.param.Context;
import org.randombits.support.core.param.ParameterInterpreter;

import java.lang.annotation.*;

/**
 * Provides details about the {@link org.randombits.filtering.core.criteria.Criterion} values that are used by {@link MultipleSearchQueryInterpreter}.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Context
public @interface MultipleSearchQueryValues {

    /**
     * The {@link org.randombits.support.core.param.ParameterInterpreter}, which must interpret into some form of {@link org.randombits.filtering.core.criteria.Criterion}.
     *
     * @return
     */
    Class<? extends ParameterInterpreter<? extends SearchQuery>> value();
}
