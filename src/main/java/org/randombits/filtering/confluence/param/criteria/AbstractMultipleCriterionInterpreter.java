package org.randombits.filtering.confluence.param.criteria;

import org.randombits.filtering.core.criteria.*;
import org.randombits.support.core.param.AbstractMultipleParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.util.Set;

/**
 * An abstract base class for {@link org.randombits.support.core.param.ParameterInterpreter}s
 * that can be converted into {@link Criterion} or {@link Criteria}. It provides support for creating
 * a complex {@link Criterion} that allows for '+/-' flagging and groups using parentheses. This class
 * handles all the group parsing, subclasses simply need to provide the implementation
 * {@link #createInstance(String, org.randombits.support.core.param.ParameterContext)} which should
 * create a single instance of the {@link Criterion}.
 */
public abstract class AbstractMultipleCriterionInterpreter extends AbstractMultipleParameterInterpreter<Criterion> {

    @Override
    public Criterion createGroup( Set<? extends Criterion> must, Set<? extends Criterion> should, Set<? extends Criterion> mustNot,
                                  ParameterContext context ) {
        Criteria criteria;
        if ( !mustNot.isEmpty() ) {
            Criterion mustNotCriterion = new NotCriterion( new AndCriteria( asArray( mustNot ) ) );

            if ( !must.isEmpty() ) {
                criteria = new AndCriteria( asArray( must ) );
            } else if ( !should.isEmpty() ) {
                criteria = new AndCriteria();
                criteria.addCriterion( new OrCriteria( asArray( should ) ) );
            } else {
                return getTargetType().cast( mustNotCriterion );
            }

            criteria.addCriterion( mustNotCriterion );
        } else if ( !must.isEmpty() )
            criteria = new AndCriteria( asArray( must ) );
        else
            criteria = new OrCriteria( asArray( should ) );

        return getTargetType().cast( criteria );
    }

    private Criterion[] asArray( Set<? extends Criterion> criteria ) {
        return criteria.toArray( new Criterion[criteria.size()] );
    }

    @Override
    public abstract Criterion createInstance( String inputValue, ParameterContext context ) throws InterpretationException;
}
