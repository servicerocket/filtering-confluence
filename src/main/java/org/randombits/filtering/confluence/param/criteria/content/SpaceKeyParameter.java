/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.criteria.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ListQuery;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.spring.container.ContainerManager;
import org.randombits.filtering.confluence.criteria.content.SpaceKeyCriterion;
import org.randombits.filtering.confluence.param.ListQueryParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.MacroInfo;

import java.util.List;

/**
 * Filters based on the space.
 *
 * @author David Peterson
 */
public class SpaceKeyParameter extends CriterionParameter implements ListQueryParameter<Criterion> {

    public static final String[] NAMES = new String[]{"space", "spaces"};

    public static final String SELF_SPACE = SpaceKeyCriterion.Interpreter.SELF_SPACE;

    public static final String ALL_SPACES = SpaceKeyCriterion.ALL_SPACES;

    public static final String GLOBAL_SPACES = SpaceKeyCriterion.GLOBAL_SPACES;

    public static final String PERSONAL_SPACES = SpaceKeyCriterion.PERSONAL_SPACES;

    public static final String FAVOURITE_SPACES = SpaceKeyCriterion.FAVOURITE_SPACES;

    public static final String FAVORITE_SPACES = SpaceKeyCriterion.FAVORITE_SPACES;

    private SpaceManager spaceManager;

    /**
     * Constructs a space filter defaulting to {@link #SELF_SPACE}.
     */
    public SpaceKeyParameter() {
        this( SELF_SPACE );
    }

    public SpaceKeyParameter( String defaultValue ) {
        this( NAMES, defaultValue );
    }

    public SpaceKeyParameter( String[] names, String defaultValue ) {
        super( names, defaultValue );
    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new SpaceKeyCriterion.Interpreter( getSpace( info.getContent() ) );
    }

    private Space getSpace( ContentEntityObject content ) {
        if ( content instanceof SpaceContentEntityObject ) {
            return ( (SpaceContentEntityObject) content ).getSpace();
        }
        return null;
    }

    /**
     * Prepares the specified list query to apply the settings in this
     * parameter.
     *
     * @param listQuery The list query.
     * @param info      The macro info for the current context.
     * @throws ParameterException if there is a problem while applying the parameter settings.
     */
    public void prepareListQuery( ListQuery listQuery, MacroInfo info ) throws ParameterException {
        Criterion criterion = createCriterion( info );

        if ( criterion != null ) {
            List<Space> allSpaces = getSpaceManager().getAllSpaces();
            List<Space> matchingSpaces = new java.util.ArrayList<Space>();

            for ( Space space : allSpaces ) {
                if ( criterion.matches( space ) )
                    matchingSpaces.add( space );
            }

            listQuery.setSpaceList( matchingSpaces );
        } else {
            listQuery.setSpaceList( null );
        }
    }

    private SpaceManager getSpaceManager() {
        if ( spaceManager == null )
            spaceManager = (SpaceManager) ContainerManager.getComponent( "spaceManager" );

        return spaceManager;
    }
}
