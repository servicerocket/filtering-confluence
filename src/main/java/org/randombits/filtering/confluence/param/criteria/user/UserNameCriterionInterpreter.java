package org.randombits.filtering.confluence.param.criteria.user;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import org.randombits.filtering.confluence.criteria.user.AnonymousUserCriterion;
import org.randombits.filtering.confluence.criteria.user.UserCriterion;
import org.randombits.filtering.confluence.criteria.user.UserNameCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.filtering.core.criteria.NotCriterion;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a user-related criterion.
 */
public class UserNameCriterionInterpreter extends AbstractParameterInterpreter<Criterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<UserNameCriterionInterpreter> {

        public Multiple() {
            super( new UserNameCriterionInterpreter() );
        }
    }

    public static final String SELF = "@self";

    public static final String ANONYMOUS = "@anonymous";

    public static final String AUTHENTICATED = "@authenticated";

    @Override
    public Criterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        if ( SELF.equals( inputValue ) ) {
            return new UserCriterion( AuthenticatedUserThreadLocal.getUser() );
        } else if ( ANONYMOUS.equals( inputValue ) ) {
            return new AnonymousUserCriterion();
        } else if ( AUTHENTICATED.equals( inputValue ) ) {
            return new NotCriterion( new AnonymousUserCriterion() );
        } else if ( inputValue.indexOf( '*' ) >= 0 ) {
            // Convert the simple pattern match to a real regexp.
            inputValue = inputValue.replaceAll( "\\.", "\\." ).replaceAll( "\\*", ".*" );
            return new UserNameCriterion( inputValue );
        } else {
            return new UserCriterion( inputValue );
        }
    }

}
