/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param;

import org.randombits.storage.Storage;
import org.randombits.support.confluence.MacroInfo;

import java.util.Arrays;
import java.util.List;

/**
 * A base implementation of {@link Parameter}, providing support for multiple
 * default parameter names, and a default value if no parameter is specified.
 * <p/>
 * <p/>
 * Additional aliases for parameter names can be added with the
 * {@link #addParameterAlias(String)} method.
 *
 * @author David Peterson
 */
public abstract class BaseParameter<T> implements Parameter<T> {

    private List<String> paramNames;

    private String defaultValue;

    public BaseParameter( String[] names, String defaultValue ) {
        paramNames = new java.util.ArrayList<String>( Arrays.asList( names ) );
        this.defaultValue = defaultValue;
    }

    public BaseParameter( List<String> names, String defaultValue ) {
        paramNames = new java.util.ArrayList<String>( names );
        this.defaultValue = defaultValue;
    }

    public void addParameterAlias( String name ) {
        paramNames.add( name );
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    protected String getParameter( Storage params, List<String> names, String defaultValue ) {
        String value = null;
        for ( String param : names ) {
            value = params.getString( param, null );
            if ( value != null )
                break;
        }

        if ( value == null )
            return defaultValue;

        return value;
    }

    public T findValue( MacroInfo info ) throws ParameterException {
        String paramValue = getParameter( info.getMacroParams(), getParameterAliases(), defaultValue );
        return findObject( paramValue, info );
    }

    public List<String> getParameterAliases() {
        return paramNames;
    }

    protected abstract T findObject( String paramValue, MacroInfo info ) throws ParameterException;
}
