/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.criteria.user;

import org.randombits.filtering.confluence.criteria.user.SpacePermissionCriterion;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.MacroInfo;

/**
 * Constructs a new criterion with a space key/permission shortcut delimiter.
 * <p/>
 * <p/>
 * The 'space' value should have the following form: "spacekey[>perm]", where
 * 'perm' may be one of the following:
 * <ul>
 * <li><i>view</i> (the default)</li>
 * <li><i>edit</i></li>
 * <li><i>admin</i></li>
 * <li><i>blog</i></li>
 * <li><i>news</i></li>
 * <li><i>comment</i></li>
 * </ul>
 * <p/>
 * <p/>
 * For example, to specify the 'DEMO' space with view permission, the value
 * should be the following: "DEMO:view" or just "DEMO" (since 'view' is the
 * default) <p/>
 * <p/>
 * To specify edit permissions in the same space, use the following:
 * "DEMO:edit".
 *
 * @author David Peterson
 */
public class SpacePermissionParameter extends CriterionParameter {

    public static final String[] NAMES = new String[]{"space", "spaces"};

    public SpacePermissionParameter( String[] names, String defaultValue ) {
        super( names, defaultValue );
    }

    public SpacePermissionParameter( String defaultValue ) {
        this( NAMES, defaultValue );
    }

    public SpacePermissionParameter() {
        this( null );

    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new SpacePermissionCriterion.Interpreter( info.getConversionContext().getSpaceKey() );
    }
}
