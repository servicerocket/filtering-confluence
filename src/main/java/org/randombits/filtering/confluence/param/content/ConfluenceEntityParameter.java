/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.content;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import org.randombits.filtering.confluence.param.BaseParameter;
import org.randombits.filtering.confluence.param.ParameterException;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroInfo;

/**
 * This parameter will return a ContentEntityObject for a given link address.
 *
 * @author David Peterson
 */
public class ConfluenceEntityParameter extends BaseParameter<ConfluenceEntityObject> {

    public static final String SELF = "@self";

    public static final String PARENT = "@parent";

    public static final String HOME = "@home";

    private final LinkAssistant linkAssistant;

    public ConfluenceEntityParameter( String[] names, String defaultValue, LinkAssistant linkAssistant ) {
        super( names, defaultValue );
        this.linkAssistant = linkAssistant;
    }

    @Override
    protected ConfluenceEntityObject findObject( String paramValue, MacroInfo info )
            throws ParameterException {
        if ( paramValue == null )
            return null;
        if ( SELF.equals( paramValue ) )
            return info.getContent();
        else if ( PARENT.equals( paramValue ) ) {
            if ( info.getContent() instanceof Page )
                return ( (Page) info.getContent() ).getParent();
            else
                return null;
        } else if ( HOME.equals( paramValue ) ) {
            if ( info.getContent() instanceof SpaceContentEntityObject ) {
                Space space = ( (SpaceContentEntityObject) info.getContent() ).getSpace();
                if ( space != null ) {
                    return space.getHomePage();
                }
            }

            return null;
        }

        return linkAssistant.getEntityForWikiLink( info.getConversionContext(), paramValue );
    }
}
