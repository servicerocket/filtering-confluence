package org.randombits.filtering.confluence.param.criteria.user;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import org.randombits.filtering.confluence.criteria.user.SpacePermissionCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.util.Map;

/**
 * Interprets a parameter String as a {@link SpacePermissionCriterion} instance.
 */
public class SpacePermissionCriterionInterpreter extends AbstractParameterInterpreter<SpacePermissionCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<SpacePermissionCriterionInterpreter> {

        public Multiple() {
            super( new SpacePermissionCriterionInterpreter() );
        }
    }

    /**
     * A post-fix for specifying a user must have 'edit' permissions for a
     * spaces.
     */
    public static final String EDIT_PAGE = "edit";


    /**
     * A post-fix for specifying a user must have 'admin' permissions for a
     * space.
     */
    public static final String SPACE_ADMIN = "admin";

    // This is private because blog has been deprecated in favour of news.
    private static final String SPACE_BLOG = "blog";

    /**
     * A post-fix for specifying a user must have 'news' permissions for a
     * space.
     */
    public static final String SPACE_NEWS = "news";

    /**
     * A post-fix for specifying a user must have 'view' permissions for a
     * space.
     */
    public static final String SPACE_VIEW = "view";

    /**
     * A post-fix for specifying a user must have 'comment' permissions for
     * a space.
     */
    public static final String SPACE_COMMENT = "comment";

    private static final String SELF_SPACE = "@space";

    private static final Map<String, Object> ACTIONS;

    static {
        ACTIONS = new java.util.HashMap<String, Object>( 4 );
        ACTIONS.put( SPACE_VIEW, Permission.VIEW );
        ACTIONS.put( SPACE_ADMIN, Permission.ADMINISTER );

        ACTIONS.put( EDIT_PAGE, Page.class );
        ACTIONS.put( SPACE_BLOG, BlogPost.class );
        ACTIONS.put( SPACE_NEWS, BlogPost.class );
        ACTIONS.put( SPACE_COMMENT, Comment.class );
    }

    @Override
    public SpacePermissionCriterion interpret( String value, ParameterContext context ) throws InterpretationException {
        Object action;

        String spaceKey = value;

        // Find the separator.
        int splitAt = spaceKey.indexOf( '>' );
        if ( splitAt == -1 )
            splitAt = spaceKey.indexOf( ':' );

        if ( splitAt != -1 ) {
            action = ACTIONS.get( spaceKey.substring( splitAt + 1 ).toLowerCase().trim() );
            spaceKey = spaceKey.substring( 0, splitAt ).trim();

            if ( action == null )
                throw new IllegalArgumentException( "The permission requested is not recognised: " + value );
        } else {
            action = Permission.VIEW;
        }

        if ( SELF_SPACE.equals( spaceKey ) ) {
            ConversionContext cc = context.get( ConversionContext.class, null );

            if ( cc != null && cc.getSpaceKey() != null )
                spaceKey = cc.getSpaceKey();
            else
                throw new InterpretationException( SELF_SPACE + " cannot be used in this context." );
        }

        if ( action instanceof Permission ) {
            return new SpacePermissionCriterion( spaceKey, (Permission) action );
        } else if ( action instanceof Class ) {
            return new SpacePermissionCriterion( spaceKey, (Class<?>) action );
        } else {
            throw new InterpretationException( "Unsupported space permission: " + value );
        }
    }

}
