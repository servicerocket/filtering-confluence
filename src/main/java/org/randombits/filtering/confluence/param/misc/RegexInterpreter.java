package org.randombits.filtering.confluence.param.misc;

import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

import java.util.regex.Pattern;

/**
 * Interprets a parameter String as a Regular Expression {@link Pattern}.
 */
public class RegexInterpreter extends AbstractParameterInterpreter<Pattern> {

    @Override
    public Pattern interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        Pattern pattern = null;
        if ( inputValue != null ) {
            inputValue = inputValue.replaceAll( ",\\s*", "|" );
            pattern = Pattern.compile( inputValue );
        }
        return pattern;

    }
}
