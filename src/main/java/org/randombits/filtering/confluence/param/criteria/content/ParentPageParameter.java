package org.randombits.filtering.confluence.param.criteria.content;

import org.randombits.filtering.confluence.criteria.content.ParentPageCriterion;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroInfo;

public class ParentPageParameter extends CriterionParameter {

    public static final String[] NAMES = new String[]{"parent", "parents"};

    private final LinkAssistant linkAssistant;

    public ParentPageParameter( LinkAssistant linkAssistant ) {
        this( null, linkAssistant );
    }

    public ParentPageParameter( String defaultValue, LinkAssistant linkAssistant ) {
        super( NAMES, defaultValue );
        this.linkAssistant = linkAssistant;
    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new ParentPageCriterion.Interpreter( linkAssistant, info.getContent() );
    }

}
