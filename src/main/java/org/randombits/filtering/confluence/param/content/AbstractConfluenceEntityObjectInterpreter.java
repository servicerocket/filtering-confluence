/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.content;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * This parameter will return a ContentEntityObject for a given link address.
 *
 * @author David Peterson
 */
public abstract class AbstractConfluenceEntityObjectInterpreter<T extends ConfluenceEntityObject> extends AbstractParameterInterpreter<T> {

    public static final String SELF = "@self";

    public static final String PARENT = "@parent";

    public static final String HOME = "@home";

    private final LinkAssistant linkAssistant;

    private final EnvironmentAssistant environmentAssistant;

    public AbstractConfluenceEntityObjectInterpreter( LinkAssistant linkAssistant, EnvironmentAssistant environmentAssistant ) {
        this.linkAssistant = linkAssistant;
        this.environmentAssistant = environmentAssistant;
    }

    @Override
    public T interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        ConversionContext cc = context.get( ConversionContext.class, null );
        if ( cc == null )
            cc = environmentAssistant.getValue( ConversionContext.class );
        if ( cc == null )
            cc = new DefaultConversionContext( new PageContext() );

        ContentEntityObject content = cc.getEntity();

        ConfluenceEntityObject value = null;

        if ( inputValue != null ) {
            if ( SELF.equals( inputValue ) ) {
                value = content;
            } else if ( PARENT.equals( inputValue ) ) {
                if ( content instanceof Page )
                    value = ( (Page) content ).getParent();
            } else if ( HOME.equals( inputValue ) ) {
                if ( content instanceof SpaceContentEntityObject ) {
                    Space space = ( (SpaceContentEntityObject) content ).getSpace();
                    if ( space != null ) {
                        value = space.getHomePage();
                    }
                }
            } else {
                value = linkAssistant.getEntityForWikiLink( cc, inputValue );
            }
        }

        return getTargetType().isInstance( value ) ? getTargetType().cast( value ) : null;
    }
}
