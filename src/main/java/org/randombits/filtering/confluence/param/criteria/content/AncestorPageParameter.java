package org.randombits.filtering.confluence.param.criteria.content;

import org.randombits.filtering.confluence.criteria.content.AncestorPageCriterion;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.confluence.MacroInfo;

public class AncestorPageParameter extends CriterionParameter {

    private final LinkAssistant linkAssistant;

    public AncestorPageParameter( String defaultValue, LinkAssistant linkAssistant ) {
        super( new String[]{"ancestor", "ancestors"}, defaultValue );
        this.linkAssistant = linkAssistant;
    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new AncestorPageCriterion.Interpreter( linkAssistant, info.getContent() );
    }

}
