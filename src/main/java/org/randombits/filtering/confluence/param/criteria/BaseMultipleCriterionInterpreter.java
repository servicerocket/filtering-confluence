package org.randombits.filtering.confluence.param.criteria;

import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 * Provides a base class that will create a 'multiple' {@link Criterion} parameter based
 * on the provided {@link ParameterInterpreter} which also supports {@link Criterion}.
 */
public class BaseMultipleCriterionInterpreter<I extends ParameterInterpreter<? extends Criterion>> extends AbstractMultipleCriterionInterpreter {

    private final I instanceInterpreter;

    public BaseMultipleCriterionInterpreter( I instanceInterpreter ) {
        this.instanceInterpreter = instanceInterpreter;
    }

    @Override
    public Criterion createInstance( String inputValue, ParameterContext context ) throws InterpretationException {
        return instanceInterpreter.interpret( inputValue, context, Criterion.class );
    }
}
