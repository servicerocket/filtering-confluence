package org.randombits.filtering.confluence.param.criteria.global;

import org.randombits.filtering.confluence.criteria.global.BuildNumberCriterion;
import org.randombits.filtering.confluence.param.criteria.CriterionParameter;
import org.randombits.filtering.core.criteria.CriterionInterpreter;
import org.randombits.support.confluence.MacroInfo;

public class BuildNumberParameter extends CriterionParameter {

    public static final String[] NAMES = new String[]{"build", "builds", "buildNumber", "buildNumbers"};

    public BuildNumberParameter() {
        this( null );
    }

    protected BuildNumberParameter( String defaultValue ) {
        super( NAMES, defaultValue );
    }

    @Override
    protected CriterionInterpreter createCriterionInterpreter( MacroInfo info ) {
        return new BuildNumberCriterion.Interpreter();
    }

}
