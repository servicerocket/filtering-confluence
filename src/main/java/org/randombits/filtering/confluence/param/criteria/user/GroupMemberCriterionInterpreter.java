package org.randombits.filtering.confluence.param.criteria.user;

import org.randombits.filtering.confluence.criteria.user.GroupMemberCriterion;
import org.randombits.filtering.confluence.param.criteria.BaseMultipleCriterionInterpreter;
import org.randombits.support.core.param.AbstractParameterInterpreter;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterContext;

/**
 * Interprets a parameter String as a {@link GroupMemberCriterion}.
 */
public class GroupMemberCriterionInterpreter extends AbstractParameterInterpreter<GroupMemberCriterion> {

    public static class Multiple extends BaseMultipleCriterionInterpreter<GroupMemberCriterionInterpreter> {

        public Multiple() {
            super( new GroupMemberCriterionInterpreter() );
        }
    }

    @Override
    public GroupMemberCriterion interpret( String inputValue, ParameterContext context ) throws InterpretationException {
        return new GroupMemberCriterion( inputValue );
    }
}
