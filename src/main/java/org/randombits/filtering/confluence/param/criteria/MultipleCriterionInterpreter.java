package org.randombits.filtering.confluence.param.criteria;

import org.randombits.filtering.core.criteria.Criterion;
import org.randombits.support.core.param.InterpretationException;
import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.ParameterInterpreter;

/**
 *
 */
public class MultipleCriterionInterpreter extends AbstractMultipleCriterionInterpreter {

    private final ParameterAssistant parameterAssistant;

    public MultipleCriterionInterpreter( ParameterAssistant parameterAssistant ) {
        this.parameterAssistant = parameterAssistant;
    }

    @Override
    public Criterion createInstance( String inputValue, ParameterContext context ) throws InterpretationException {
        MultipleCriterionValues criterionValues = context.getAnnotation( MultipleCriterionValues.class );
        if ( criterionValues == null )
            throw new InterpretationException( "Please provide a @MultipleSearchQueryValues annotation on this parameter." );

        ParameterInterpreter<? extends Criterion> interpreter = parameterAssistant.findParameterInterpreter( criterionValues.value() );

        if ( interpreter == null )
            throw new InterpretationException( "Ensure the following interpreter has a <parameter-interpreter> module in 'atlassian-plugin.xml': " + criterionValues.value().getName() );

        return interpreter.interpret( inputValue, context, Criterion.class );
    }
}
