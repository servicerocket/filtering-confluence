/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.filtering.confluence.param.format;

import com.atlassian.confluence.util.GeneralUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A utility class describing the pre-, mid- and postfix for a given separator.
 * 
 * @author David Peterson
 */
public class Separator {

    public enum Type {
        /** Square brackets [] */
        BRACKETS("bracket", "[ ", " ] [ ", " ]"),
        /** Curly braces {} */
        BRACES("brace", "{ ", " } { ", " }"),
        /** Parentheses () */
        PARENTHESES("paren", "( ", " ) ( ", " )"),
        /** Vertical pipes */
        PIPES("pipe", " | "),
        /** COMMAS "," */
        COMMAS("comma", ", "),
        /** line breaks. */
        NEW_LINES("newline", "<br/>\n");

        private final String name;

        private final String prefix;

        private final String midfix;

        private final String postfix;

        Type( String name, String midfix ) {
            this( name, "", midfix, "" );
        }

        Type( String name, String prefix, String midfix, String postfix ) {
            this.name = name;
            this.prefix = prefix;
            this.midfix = midfix;
            this.postfix = postfix;
        }

        public boolean matches( String name ) {
            return name != null && name.toLowerCase().startsWith( this.name );
        }

        public static Type forName( String name ) {
            for ( Type type : Type.values() ) {
                if ( type.matches( name ) )
                    return type;
            }
            return null;
        }

        public String getName() {
            return name;
        }
    }

    private static final Pattern SEPARATOR_QUOTED = Pattern.compile( "[\"']?([^\"']*)[\"']?" );

    private String pre;

    private String mid;

    private String post;

    public Separator( Type type ) {
        this.pre = type.prefix;
        this.mid = type.midfix;
        this.post = type.postfix;
    }

    public Separator( String pre, String mid, String post ) {
        this.pre = pre;
        this.mid = mid;
        this.post = post;
    }

    public String getPre() {
        return pre;
    }

    public String getMid() {
        return mid;
    }

    public String getPost() {
        return post;
    }

    public static Separator parse( String separator ) {

        if ( separator != null ) {
            Type type = Type.forName( separator );

            if ( type != null ) {
                return new Separator( type );
            } else {
                Matcher matcher = Separator.SEPARATOR_QUOTED.matcher( separator );
                if ( matcher.matches() ) {
                    String midfix = GeneralUtil.htmlEncode( matcher.group( 1 ) );
                    return new Separator( "", midfix, "" );
                } else {
                    throw new IllegalArgumentException( "Unsupported separator value: " + separator );
                }
            }

        }

        return null;
    }

}
