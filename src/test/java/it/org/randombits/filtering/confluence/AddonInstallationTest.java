package it.org.randombits.filtering.confluence;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Mohd Faiz Hasim
 * @since 1.2.5.20141014
 */
public class AddonInstallationTest extends AbstractWebDriverTest {

    @Test public void shouldAbleToInstallAddonInConfluence() throws Exception {
        boolean isInstalled = new AddOnTest().isInstalled(
            product.login(User.ADMIN, AddOnOSGIPage.class),
            "RB Filtering - Confluence"
        );

        assertThat("`RB Filtering - Confluence` should be successfully installed.", isInstalled);
    }
}
